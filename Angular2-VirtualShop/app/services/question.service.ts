import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Question } from '../Question';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  url: string = 'http://localhost:9000/question/';
  constructor(private http: Http) { }

  addQuestion(question: Question) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.url + 'add',question).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }

   getQuestion(productId: number) {
    return this.http.get(this.url + 'product/' + productId).pipe(map((response:Response)=>response.json()));
   }
}
