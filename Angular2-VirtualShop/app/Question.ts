import { User } from "./user";
import { Product } from "./product";

export class Question {
    questionId:number;
    user: User;
    product:Product;
    questionContent:string;
}