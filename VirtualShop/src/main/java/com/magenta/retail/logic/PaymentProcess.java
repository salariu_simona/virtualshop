package com.magenta.retail.logic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.magenta.retail.display.DisplayData;
import com.magenta.retail.money.Money;
import com.magenta.retail.money.MoneyStock;
import com.magenta.retail.utils.Utils;

/**
 * This class provides all the necessary method in the customer's payment process, payment method in differents types of currency and rest of money functionality
 *
 */
public class PaymentProcess {

	private ReaderClass input;
	private MoneyStock moneyStock;
	private static double shopGain = 0;
	private List<Money> clientRestofMoney;
	private DisplayData displayData;
	

	public PaymentProcess() {
		input = new ReaderClass();
		displayData = new DisplayData();
		moneyStock = new MoneyStock();
		moneyStock.initializeStockOfMoney();
		clientRestofMoney = new ArrayList<Money>();
		initListForClientRest();
		
	}
	// method for initialize a list which contains the rest of money for client
	public void initListForClientRest() {
		for (Map.Entry<Integer, Money> money : moneyStock.getStockOfMoney().entrySet()) {
			clientRestofMoney.add(new Money(0,money.getValue().getValue()));
		}
	}
	
	// set quantity 0 for each money from clientRestOfMoney list
	public void setQuantityFromClientRest() {
		for(int i=0; i < clientRestofMoney.size();i++) {
			clientRestofMoney.get(i).setQuantity(0);
		}
	}
	public Properties initProperties() {
		Properties properties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(Utils.CURRENCY_TYPES_FILE_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}

	// method for formatting a number with many decimal into a number with two decimal
	public double formatNumber(double number) {
		DecimalFormat format = new DecimalFormat("#.##");
		return Double.valueOf(format.format(number));
	}

	// products payment in RON
	public void paymentProcessWithRon(double totalPrice) {
		setQuantityFromClientRest();
		double price = totalPrice;
		double remainingSum = 0;
		List<Money> copyList = moneyStock.getCopyStockOfMoney();
		do {
			displayData.printMessage(Utils.CHOICE_MONEY_MESSAGE);
			int moneyIndex = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, 11);
			displayData.printMessage(Utils.BILLS_AND_COINS_CHOICE_MESSAGE);
			int numberOfCoinsOrBills = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 0, Integer.MAX_VALUE);
			moneyStock.addMoneyToStock(moneyIndex, numberOfCoinsOrBills);
			remainingSum = formatNumber(totalPrice - moneyStock.getCurrentSum(moneyIndex, numberOfCoinsOrBills));
			if (remainingSum == 0) {
				successfulTransaction(price);
			} else if (remainingSum < 0) {
				displayData.printMessage(Utils.REST_OF_MONEY_MESSAGE + Math.abs(remainingSum) + " lei.");
				if (restOfMoneyMethod(Math.abs(remainingSum)) == false) {
					moneyStock.updateStockOfMoney(copyList);
				} else {
					successfulTransaction(price);
				}
			} else if (remainingSum > 0) {
				displayData.printMessage(Utils.REMAINING_SUM_MESSAGE + remainingSum + " lei.");
			}
			totalPrice = remainingSum;
		} while (remainingSum > 0);
	}

	// products payment in EURO/DOLLAR
	public void paymentProcess(String choice, double totalPrice) {
		setQuantityFromClientRest();
		double price = totalPrice;
		double remainingSum = 0;
		List<Money> copyList = moneyStock.getCopyStockOfMoney();
		do {
			displayData.printMessage("Introduceti suma: ");
			double clientSumOfMoney = input.readDoubleValue(Utils.INVALID_OPTION_MESSAGE, 0.0, Double.MAX_VALUE);
			double valueInMainCurrency = exchangeCurrency(choice, clientSumOfMoney);
			displayData.printMessage(Utils.SUM_IN_MAIN_CURRENCY + valueInMainCurrency + " lei.");
			moneyDivisionAndStockUpdate(valueInMainCurrency);
			remainingSum = formatNumber((totalPrice - valueInMainCurrency));
			if (remainingSum == 0) {
				successfulTransaction(price);
			} else if (remainingSum < 0) {
				displayData.printMessage(Utils.REST_OF_MONEY_MESSAGE + Math.abs(remainingSum) + " lei.");
				if (restOfMoneyMethod(Math.abs(remainingSum)) == true) {
					successfulTransaction(price);
				} else {
					moneyStock.updateStockOfMoney(copyList);
				}
			} else if (remainingSum > 0) {
				displayData.printMessage(Utils.REMAINING_SUM_MESSAGE + remainingSum + " lei.");
			}
			totalPrice = remainingSum;
		} while (remainingSum > 0);
	}

	// method for exchange currency if the client want to pay in EURO/DOLLAR
	public double exchangeCurrency(String type, double value) {
		Properties prop = initProperties();
		double conversionInMainCurrency = 0;
		if (type.equals("Euro")) {
			conversionInMainCurrency = value * Double.parseDouble(prop.getProperty("exchangeEuro"));
		} else if (type.equals("Dollar")) {
			conversionInMainCurrency = value * Double.parseDouble(prop.getProperty("exchangeDollar"));
		}
		return Math.ceil(conversionInMainCurrency);
	}

	// the method to divide an amount of money into bills and then update the stock of money
	public void moneyDivisionAndStockUpdate(double sum) {
		for (Map.Entry<Integer, Money> money : moneyStock.getStockOfMoney().entrySet()) {
			double value = money.getValue().getValue();
			int key = money.getKey();
			if (sum == value) {
				moneyStock.addMoneyToStock(key, 1);
				sum = formatNumber(sum - value);
			} else if (sum > value) {
				while (sum >= value) {
					moneyStock.addMoneyToStock(key, 1);
					sum = formatNumber(sum - value);
				}
			}
		}
	}

	// method for calculating the rest of money for the client
	public double giveRest(double rest) {
		int number = 0;
		for (Map.Entry<Integer, Money> money : moneyStock.getStockOfMoney().entrySet()) {
			double value = money.getValue().getValue();
			int key = money.getKey();
			if (rest == value) {
				number = money.getValue().getQuantity();
				if (number > 0) {
					addToClientRest(value);
					moneyStock.removeMoneyFromStock(key, 1);			
					rest = formatNumber(rest - value);
				}
			} else if (rest > value) {
				number = money.getValue().getQuantity();
				while (rest >= value && number != 0) {
					number = money.getValue().getQuantity();
					if (number > 0) {
						addToClientRest(value);
						moneyStock.removeMoneyFromStock(key, 1);
						rest = formatNumber(rest - value);
					}
				}
			}
		}
		return rest;
	}

	// the method that checks whether the stock has the money to provide the rest to the customer
	public boolean restOfMoneyMethod(double rest) {
		double remainingRest = giveRest(rest);
		if (remainingRest != 0.0) {
			displayData.printMessage("Ne pare rau!Nu va putem achita restul!");
			return false;
		}
		return true;
	}
	
	 // adding bills/coins in a list for client
	 
	public void addToClientRest(double value) {
		int j = 0;
		while ((clientRestofMoney.get(j).getValue() != value)) {
			j++;
		}
		clientRestofMoney.get(j).setQuantity(clientRestofMoney.get(j).getQuantity() + 1);
	}

	// display the list of bills / coins offered within the rest
	public void printRestForClient() {
		displayData.printMessage("Bancnote/Monede oferite: ");
		for(int i=0; i < clientRestofMoney.size(); i++) {
			if(clientRestofMoney.get(i).getQuantity() != 0) {
				if (clientRestofMoney.get(i).getValue() <= 0.5) {			
					displayData.printMessage("*\t" + clientRestofMoney.get(i).getQuantity() + "x" + (int)(clientRestofMoney.get(i).getValue()*100) + " bani");
				} else if (clientRestofMoney.get(i).getValue() == 1.0){
					displayData.printMessage("*\t" + clientRestofMoney.get(i).getQuantity() + "x" + (int)clientRestofMoney.get(i).getValue() + " leu");
				} else {
					displayData.printMessage("*\t" + clientRestofMoney.get(i).getQuantity() + "x" + (int)clientRestofMoney.get(i).getValue() + " lei");
				}
			}
		}
	}
	
	// display the list if transaction was successful finished
	public void successfulTransaction(double totalPrice) {
		shopGain += totalPrice;
		printRestForClient();
		displayData.printMessage("Profit magazinului: " + formatNumber(shopGain) + " lei.");
	}
}
