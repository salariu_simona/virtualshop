import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Answer } from '../Answer';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  url: string = 'http://localhost:9000/answer/';

  constructor(private http: Http) { }

  addAnswer(answer: Answer) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.url + 'add',answer).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }

   getAnswers(questionId: number) {
    return this.http.get(this.url + 'question/' + questionId).pipe(map((response:Response)=>response.json()));
   }
}
