package com.magenta.retail.money;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.Test;

class BLMoneyReaderTest {

	@Test
	public void readFileTest() {
		BLIMoneyReader moneyReader = new BLMoneyReader();
			try {
				moneyReader.readFile("file_does_not_exists.txt");
			}catch(Exception e) {
				assertThat(e,instanceOf(NullPointerException.class));
			}
		}
	
}
