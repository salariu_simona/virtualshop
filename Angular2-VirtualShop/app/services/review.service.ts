import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Review } from '../review';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  url: string = 'http://localhost:9000/review/';

  constructor(private http: Http) { }

  addReview(review: Review) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.url + 'add',review).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }

   getReview(productId: number) {
    return this.http.get(this.url + 'product/' + productId).pipe(map((response:Response)=>response.json()));
   }
}
