package com.magenta.retail.question;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("question")
public class QuestionController {
	@Autowired
	private BLQuestionService questionService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	private void addQuestion(@RequestBody Question question) {
		questionService.addQuestion(question);
	}
	@RequestMapping(value = "/product/{product_id}", method = RequestMethod.GET) 
	private List<Question> getQuestionByProduct(@PathVariable("product_id") int product_id){
		return questionService.getQuestionForProduct(product_id);
	}
}
