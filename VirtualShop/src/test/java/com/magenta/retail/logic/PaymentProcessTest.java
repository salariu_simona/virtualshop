package com.magenta.retail.logic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.magenta.retail.logic.PaymentProcess;

class PaymentProcessTest {

	PaymentProcess paymentProcess = new PaymentProcess();
	@Test
	void formatNumberTest() {
		assertEquals(12.12,paymentProcess.formatNumber(12.123));
	}
	
	@Test
	void exchangeCurrencyEuroTest() {
		assertEquals(10,paymentProcess.exchangeCurrency("Euro", 2));
	}
	@Test
	void exchangeCurrencyDollarTest() {
		assertEquals(8,paymentProcess.exchangeCurrency("Dollar", 2));
	}
}
