package com.magenta.retail.product;



/**
 * Creating Product with name, quantity and price fields using Builder design pattern
 */

public class Product {

	private String name;
	private int quantity;
	private double price;

	
	public Product(ProductBuilder product) {
		name = product.name;
		quantity = product.quantity;
		price = product.price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public String getName() {
		return name;
	}
	
	public static class ProductBuilder {
		private final String name;
		private int quantity;
		private double price;
		
		
		public ProductBuilder(String name) {
			this.name = name;
		}
		public ProductBuilder quantity(int quantity) {
			this.quantity = quantity;
			return this;
		}
		public ProductBuilder price(double price) {
			this.price = price;
			return this;
		}
		
		public Product build() {
			return new Product(this);
		}
	}
	
	public String toString() {
		return "Name: " + this.name + ", Quantity: " + this.quantity + ", Price: " + this.price; 
		
	}

}
