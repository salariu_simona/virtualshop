package com.magenta.retail.customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BLCustomerService implements BLICustomerService {

	@Autowired
	private CustomerRepository customer_repository;
	public void addCustomer(Customer customer) {
		customer_repository.save(customer);
	}
	public List<Customer> getCustomers() {
		return (List<Customer>) customer_repository.findAll();
	}
	
	
	@Override
	public void deleteCustomerById(int customer_id) {
		// TODO Auto-generated method stub
		
	}
	public Customer getCustomerByEmailAndPassword(Customer customer) {
		Customer new_customer = customer_repository.findByemail(customer.getEmail());
		if(new_customer != null) {
			if(new_customer.getPassword().equals(customer.getPassword())) {
				return new_customer;
			}
		}
		return null;
	}
	
	public Customer getCustomerByEmail(String email) {
		return customer_repository.findByemail(email);
	}
	public Customer getCustomerById(int customer_id) {
		return customer_repository.findBycustomerId(customer_id);	
	}
	
	public void updateCustomer(Customer customer) {
		customer_repository.save(customer);
		
	}
	
	public void deleteAllCustomers() {
		customer_repository.deleteAll();
	}

	
}
