package com.magenta.retail.utils;

/**
 * This class provides all the necessary messages for customer interaction with the application
 *
 */
public class Utils {
	public static final String PRODUCTS_FILE_PATH = "src/main/resources/products.txt";
	public static final String MONEY_FILE_PATH = "src/main/resources/money.txt";
	public static final String CURRENCY_TYPES_FILE_PATH = "src/main/resources/exchangeCurrency.properties";
	public static final String WELCOME_MESSAGE = "****************BINE ATI VENIT!****************";
	public static final String SHOPPING_PRODUCTS_MESSAGE = "\nDoriti sa achizitionati unul sau mai multe produse? \n1 -> DA \n2 -> NU";
	public static final String SHOPPING_ANOTHER_PRODUCTS_MESSAGE = "Doriti sa achizitionati si alte produse? \n1 -> DA \n2 -> NU";
	public static final String INVALID_OPTION_MESSAGE = "Optiune invalida!\nReintroduceti: ";
	public static final String LIST_OF_AVAILABLE_PRODUCTS = "\nProdusele disponibile in magazin sunt: ";
	public static final String CURRENCY_OPTION_MESSAGE = "In ce valuta doriti sa efectuati plata? \n1 -> RON \n2 -> EURO \n3 -> DOLLAR";
	public static final String PAYMENT_CONFIRMATION_MESSAGE = "\nDoriti sa finalizati comanda? \n1 -> DA \n2 -> NU";
	public static final String STOCK_NOT_AVAILABLE_MESSAGE = "Produsul nu mai este disponibil in stoc!\nDoriti sa achizitionati alt produs? \n1 -> DA\n2 -> NU";
	public static final String INVALID_QUANTITY_MESSAGE = "Cantitate invalida!Stoc insuficient!\nReintroduceti:";
	public static final String CHOICE_PRODUCTS_INDEX_MESSAGE = "Introduceti indexul produsului pe care doriti sa il achizitionati: ";
	public static final String CHOICE_PRODUCTS_QUANTITY_MESSAGE = "Introduceti cantitatea produsului pe care doriti sa il achizitionati: ";
	public static final String CHOICE_MONEY_MESSAGE = "\nBancnote: \n\t1 -> 500 lei \n\t2 -> 200 lei \n\t3 -> 100 lei \n\t4 -> 50 lei \n\t5 -> 10 lei \n\t6 -> 5 lei \n\t7 -> 1 leu"
			+ "\nMonede: \n\t8 -> 50 bani \n\t9 -> 10 bani \n\t10 -> 5 bani \n\t11 -> 1 ban";
	public static final String REMAINING_SUM_MESSAGE = "Mai aveti de platit o suma in valoare de " ;
	public static final String REST_OF_MONEY_MESSAGE = "Restul dvs. este in valoare de ";
	public static final String SUM_IN_MAIN_CURRENCY = "Suma convertita in moneda principala este in valoare de ";
	public static final String BILLS_AND_COINS_CHOICE_MESSAGE = "Introduceti numarul de bancnote/monede: ";
	
	// For Address Table
	public static final String  FIND_ADDRESS_BY_ID_MESSAGE = "Method called: findAddressById. Find Address with id = ";
	public static final String  DELETE_ADDRESS_BY_ID_MESSAGE = "Method called: deleteAddressById. Delete Address with id = ";
	public static final String  FIND_ALL_ADDRESSES_MESSAGE = "Method called: findAllAddresses";
	public static final String  INSERT_ADDRESS_MESSAGE = "Method called: insertAddress";
	public static final String  DELETE_ALL_ADDRESSES_MESSAGE = "Method called: deleteAllAddresses";
	public static final String  UPDATE_ADDRESS_MESSAGE = "Method called: updateAddress. Update Address with id = ";
	
	// For Customer Table
	
	public static final String  FIND_CUSTOMER_BY_ID_MESSAGE = "Method called: findCustomerById. Find Customer with id = ";
	public static final String  DELETE_CUSTOMER_BY_ID_MESSAGE = "Method called: deleteCustomerById. Delete Customer with id = ";
	public static final String  FIND_ALL_CUSTOMERS_MESSAGE = "Method called: findAllCustomers";
	public static final String  INSERT_CUSTOMER_MESSAGE = "Method called: insertCustomer";
	public static final String  DELETE_ALL_CUSTOMERS_MESSAGE = "Method called: deleteAllCustomers";
	public static final String  UPDATE_CUSTOMER_MESSAGE = "Method called: updateCustomer. Update Customer with id = ";
	
	// For Order Table
	public static final String  FIND_ORDER_BY_ID_MESSAGE = "Method called: findOrderById. Find Order with id = ";
	public static final String  DELETE_ORDER_BY_ID_MESSAGE = "Method called: deleteOrderById. Delete Order with id = ";
	public static final String  FIND_ALL_ORDERS_MESSAGE = "Method called: findAllOrders";
	public static final String  INSERT_ORDER_MESSAGE = "Method called: insertOrder";
	public static final String  DELETE_ALL_ORDERS_MESSAGE = "Method called: deleteAllOrders";
	public static final String  UPDATE_ORDER_MESSAGE = "Method called: updateOrder. Update Order with id = ";
	
	// For product Table
	public static final String  FIND_PRODUCT_BY_ID_MESSAGE = "Method called: findProductById. Find Product with id = ";
	public static final String  DELETE_PRODUCT_BY_ID_MESSAGE = "Method called: deleteProductById. Delete Product with id = ";
	public static final String  FIND_ALL_PRODUCTS_MESSAGE = "Method called: findAllProducts";
	public static final String  INSERT_PRODUCT_MESSAGE = "Method called: insertProduct";
	public static final String  DELETE_ALL_PRODUCTS_MESSAGE = "Method called: deleteAllProducts";
	public static final String  UPDATE_PRODUCT_MESSAGE = "Method called: updateProduct. Update Product with id = ";

	

}
