package com.magenta.retail.answer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/answer")
public class AnswerController {

	@Autowired
	private BLAnswerService answerService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	private void addAnswer(@RequestBody Answer answer) {
		answerService.addAnswer(answer);
	}
	
	@RequestMapping(value = "/question/{question_id}", method = RequestMethod.GET) 
	private List<Answer> getAnswersByQuestion(@PathVariable("question_id") int question_id){
		return answerService.getAnswersForQuestion(question_id);
	}
}
