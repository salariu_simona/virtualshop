package com.magenta.retail.product;

import java.util.List;

public interface BLIProductDAO {
	public Product findById(int product_id);
	public List<Product> findAll();
	public void deleteAll();
	public void insertProduct(Product product);
	public void updateProductr(Product product);
}
