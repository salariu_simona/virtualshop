import { User } from "./user";
import { Product } from "./product";

export class Order {
    orderId: number;
    finalPrice: number;
    paymentMethod: string;
    orderDate: string;
    deliveryAddress: string;
    user: User;
}

export class OrderProduct {
    order: Order;
    product: Product;
    quantity: number;
}