package com.magenta.retail.mail;

import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {
	private TemplateEngine templateEngine;

	@Autowired
	public MailContentBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}
	
	public String build(String message, String name) throws MessagingException {
		Context context = new Context();
		context.setVariable("message", message);
		context.setVariable("name", name);
		return this.templateEngine.process("mailTemplateOrder", context);
	}
	
	public String buildTemplateSuggestion(String message, String name) {
		Context context = new Context();
		context.setVariable("name", name);
		context.setVariable("message",message);
		return this.templateEngine.process("mailTemplateSuggestion", context);
	}
}
