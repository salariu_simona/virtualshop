package com.magenta.retail.money;

import java.util.List;

public interface BLIMoneyReader {

	public List<Money> readFile(String fileName);

}
