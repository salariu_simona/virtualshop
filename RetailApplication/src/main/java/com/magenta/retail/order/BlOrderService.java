package com.magenta.retail.order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlOrderService implements BliOrderService {
	
	@Autowired
	private OrderRepository orderRepository;

	@Override
	public Order insertOrder(Order order) {
		return orderRepository.save(order);
	}

	@Override
	public Order findOrderById(int id) {
		return orderRepository.getByorderId(id);
	}

	@Override
	public List<Order> findAllOrders() {
		return (List<Order>) orderRepository.findAll();
	}

	@Override
	public Order updateOrder(Order order) {
		Order updateOrder = orderRepository.getByorderId(order.getOrderId());
		updateOrder.setFinalPrice(order.getFinalPrice());
		updateOrder.setPaymentMethod(order.getPaymentMethod());
		return orderRepository.save(updateOrder);
	}

	@Override
	public void deleteOrderById(int id) {
		orderRepository.deleteById(id);
	}

	@Override
	public void deleteAllOrders() {
		orderRepository.deleteAll();
	}
	
	
}
