package com.magenta.retail.logic;

import java.util.Scanner;

import com.magenta.retail.display.DisplayData;

/**
 * This class provides some read methods, for different types (int/double)
 */
public class ReaderClass {

	private DisplayData displayData;
	private Scanner scan;

	public ReaderClass() {
		displayData = new DisplayData();
		scan = new Scanner(System.in);
	}

	// method for reading an integer value
	public int readIntValue(String message, int leftLimit, int rightLimit) {
		int clientChoice = 0;
		try {
			clientChoice = scan.nextInt();
			while (clientChoice < leftLimit || clientChoice > rightLimit) {
				displayData.printMessage(message);
				try {
					clientChoice = scan.nextInt();
				} catch (Exception e) {
					printError();
				}
			}
		} catch (Exception e) {
			printError();
		}
		return clientChoice;
	}
	// method for reading a double value
	public double readDoubleValue(String message, double leftLimit, double rightLimit) {
		double clientChoice = 0;
		try {
			clientChoice = scan.nextDouble();
			while (clientChoice < leftLimit || clientChoice > rightLimit) {
				displayData.printMessage(message);
				try {
					clientChoice = scan.nextDouble();
				} catch (Exception e) {
					printError();
				}
			}
		} catch (Exception e) {
			printError();
		}
		return clientChoice;
	}

	public void printError() {
		displayData.printMessage("Ne pare rau.. Aplicatia se inchide..");
		System.exit(0);
	}
}
