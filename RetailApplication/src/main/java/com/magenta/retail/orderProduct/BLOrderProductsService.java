package com.magenta.retail.orderProduct;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magenta.retail.product.Product;
import com.magenta.retail.product.ProductRepository;
import com.magenta.retail.shoppingCart.ShoppingCart;
import com.magenta.retail.shoppingCart.ShoppingCartRepository;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class BLOrderProductsService implements BLIOrderProductsService 	{

	@Autowired
	OrderProductRepository orderProductRepository;
	
	@Autowired
	OrderProductRepository orderRepository;
	
	@Autowired
	ProductRepository  productRepository;
	
	@Autowired
	ShoppingCartRepository shoppingCartRepository;
	
	public List<OrderProducts> getOrders(int order_id) {
		return orderProductRepository.findByOrderOrderId(order_id);
	}
	
	@Override
	public void add(OrderProducts orderProduct) {
		orderProductRepository.save(orderProduct);
		deleteFromShoppingCart(orderProduct.getOrder().getUser().getCustomerId(),orderProduct.getProduct().getProductId());
		generatePDFWithOrder(orderProduct.getOrder().getUser().getCustomerId(),
				orderProduct.getOrder().getOrderId());
		updateProductStock(orderProduct);	
	}
	
	public void generatePDFWithOrder(int user_id, int orderId) {
		double price = 0;
		List<ShoppingCart> shoppingCart = shoppingCartRepository.findByuserCustomerId(user_id);
		JasperPrint jasperPrint = new JasperPrint();
		Connection conn = null;
		for(int j = 0; j < shoppingCart.size(); j++) {
			price += shoppingCart.get(j).getQuantity() * shoppingCart.get(j).getProduct().getPrice();
		}
		Map<String,Object> map = new HashMap<>();
		map.put("totalToPay", Double.toString(price));
		map.put("codeBar", String.format("%09d", orderId));
		map.put("orderId", orderId);
		try {
			conn = getDataSource().getConnection();
            JasperCompileManager.compileReportToFile("src/main/resources/jasper_report_template.jrxml", "src/main/resources/jasper_report_template.jasper");
            jasperPrint = JasperFillManager.fillReport("src/main/resources/jasper_report_template.jasper",
            		map, conn);
            JasperExportManager.exportReportToPdfFile(jasperPrint, "D:\\PinkTeamWorkSpace\\RetailApplication\\src\\main\\resources\\Order.pdf");
        } catch (JRException ex) {
            ex.printStackTrace();
        } catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public DataSource getDataSource() {
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setServerName("localhost");
		dataSource.setUseSSL(false);
		dataSource.setPortNumber(3306);
		dataSource.setDatabaseName("retailDB");
		dataSource.setUser("root");
		dataSource.setPassword("magenta");
		return dataSource;
	}

	public void updateProductStock(OrderProducts orderProduct) {
		int currentStock = orderProduct.getProduct().getQuantity();
		int selectedQuantity = orderProduct.getQuantity();	
		Product product = orderProduct.getProduct();
		product.setQuantity(currentStock - selectedQuantity); 
		productRepository.save(product);
	}

	public void deleteFromShoppingCart(int userId, int productId) {
		ShoppingCart shoppingCart = shoppingCartRepository.findByuserCustomerIdAndProductProductId(userId, productId);
		shoppingCartRepository.delete(shoppingCart);
	}
	
}
