package com.magenta.retail.roles;

import java.util.List;

public interface BliRolesService {
	
	public void addRole(Roles role);
	public List<Roles> getRoles();
	public Roles getRoleById(int roleId);
	public void deleteRoleById(int roleId);
	public void updateRole(Roles role);
	public void deleteAllRoles();
}
