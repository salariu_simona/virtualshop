package com.magenta.retail.favourites;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.magenta.retail.customer.Customer;
import com.magenta.retail.product.Product;

@Entity
public class FavouriteProducts {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int favouriteProductId;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "user_id")
	private Customer user;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "product_id")
	private Product product;
	
	public FavouriteProducts() {
		super();
	}

	public int getFavouriteProductId() {
		return favouriteProductId;
	}

	public void setFavouriteProductId(int favouriteProductId) {
		this.favouriteProductId = favouriteProductId;
	}
	
	public Customer getUser() {
		return user;
	}

	public void setUser(Customer user) {
		this.user = user;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "FavouriteProducts [favouriteProductId=" + favouriteProductId + ", user=" + user + ", product=" + product
				+ "]";
	}

}
