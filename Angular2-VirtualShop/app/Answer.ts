import { Question } from "./Question";
import { User } from "./user";

export class Answer {
    answerId: number;
    question: Question;
    user: User;
    answerContent:string;
}