import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { OrderProduct } from '../Order';
import { map } from 'rxjs/operators';
import { Product } from '../product';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  url: string = 'http://localhost:9000/mail/';

  constructor(private http: Http) { }

  public sendMailWithOrder(orderProducts: OrderProduct[]) {
    return this.http.put(this.url + 'sendMailWithOrder' ,orderProducts).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json())); 
  }

  public sendMailWithSugestion(products: Product[]) {
    let user = parseInt(localStorage.getItem("userId"));
    return this.http.put(this.url + 'sendMailWithSuggestion/' + user,products).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json())); 
  }
}
