package com.magenta.retail.question;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BLQuestionService implements BLIQuestionService {

	@Autowired
	QuestionRepository questionRepository;
	@Override
	public void addQuestion(Question question) {
		questionRepository.save(question);
		
	}
	public List<Question> getQuestionForProduct(int product_id) {	
		return questionRepository.findByproductProductId(product_id);
	}

}
