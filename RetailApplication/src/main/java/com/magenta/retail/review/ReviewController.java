package com.magenta.retail.review;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/review")
public class ReviewController {

	@Autowired
	private BLReviewService reviewService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	private void addShoppingCart(@RequestBody Review review) {
		reviewService.addReview(review);
	}
	
	@RequestMapping(value = "/product/{product_id}", method = RequestMethod.GET) 
	private List<Review> getReviewByProduct(@PathVariable("product_id") int product_id){
		return reviewService.getReviewsForProduct(product_id);
	}
}
