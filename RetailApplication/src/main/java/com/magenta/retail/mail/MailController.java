package com.magenta.retail.mail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magenta.retail.orderProduct.OrderProducts;
import com.magenta.retail.product.Product;

@RestController
@RequestMapping("/mail")
public class MailController {
	
	@Autowired
	MailService mailService;
	
	@RequestMapping(value = "/sendMailWithOrder",method=RequestMethod.PUT)
	private void sendMailWithOrder(@RequestBody List<OrderProducts> orderProducts) {
		mailService.sendMailWithOrder(orderProducts);
	}
	
	@RequestMapping(value = "/sendMailWithSuggestion/{user_id}", method = RequestMethod.PUT)
	private void sendMailWithSuggestion(@PathVariable("user_id") int userId,@RequestBody List<Product> listOfSuggestions) {
		mailService.sendMailWithSuggestion(userId, listOfSuggestions);
	}
}
