import { Component, OnInit } from '@angular/core';
import { Product } from '../../product';
import { Router } from '@angular/router';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { ShoppingCart, ProductQuantityPair } from '../../shoppingcart';
import { MatDialog } from '@angular/material';
import { DialogOrderComponent } from '../dialog-order/dialog-order.component';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})

export class ShoppingCartComponent implements OnInit {
  public products: ProductQuantityPair[] = [];
  public pair: ProductQuantityPair;
  finalPrice: number = 0;
  today: Date;
  errorMessages: string[] = [];
  flags: boolean[] = [];
  constructor(private router: Router,private shopService:ShoppingCartService,public dialog: MatDialog) {
    
  }

  ngOnInit() {
    
    this.getProducts();
  }

  deleteProductFromShoppingCart(productId: number) {
    
    let userId = parseInt(localStorage.getItem("userId"));
    console.log(userId);
    this.shopService.deleteFromShoppingCart(userId,productId).subscribe((res: Response) => {
      console.log(res);
    });
    location.reload();

  }

  getProducts() {
    let user = parseInt(localStorage.getItem("userId"));
    this.shopService.getProductsFromShoppingCart(user).subscribe((shopingCart:ShoppingCart[]) => {
     
      shopingCart.forEach(element => {
        this.pair = new ProductQuantityPair();
        this.pair.product = element.product;
        this.pair.quantity = element.quantity;
        this.products.push(this.pair);
        this.total(); 
      });
    });
  }
  total() {
    if (this.products != null) {
      this.finalPrice = 0;
      this.products.forEach(product => {
          this.finalPrice = this.finalPrice + product.product.price * product.quantity;
      });
      
    }
  }

  seeProduct(productId: number) {
    localStorage.setItem("id", productId.toString());
    this.router.navigate(["productDetails"]);
  }

  openDialog(finalPrice: number) {
    console.log(this.products[0]);
    this.checkQuantity();
    if(this.flags.length == 0) {
      this.today = new Date();
      const dialogRef = this.dialog.open(DialogOrderComponent, {
        width: '500px',
        height: '400px',
        data : { price: finalPrice
               
           }});
        dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');});
    }
  }

  checkQuantity(){
    this.products.forEach(pair => {
      if(pair.product.quantity < pair.quantity) {
        this.errorMessages.push("*Only " + this.pair.product.quantity + " products available for " + this.pair.product.name) ;
        this.flags.push(false);
      } 
    });
  }
  

}
