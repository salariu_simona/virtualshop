import { HomeComponent } from "./components/home/home.component";
import { RegistrationComponent } from "./components/registration/registration.component";
import { LoginComponent } from "./components/login/login.component";
import { ProductsComponent } from "./components/products/products.component";
import { UsersComponent } from "./components/users/users.component";
import {ProductDetailsComponent} from "./components/product-details/product-details.component";
import { ShoppingCartComponent } from "./components/shopping-cart/shopping-cart.component"
import { FavouriteProductsComponent } from "./components/favourite-products/favourite-products.component";
import { LogoutComponent } from "./components/logout/logout.component";

export const AppRoutes: any = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  } ,
  {
    path:"home",
    component: HomeComponent
  },
  {
    path:"registration",
    component: RegistrationComponent
  }
  ,
  {
    path:"login",
    component: LoginComponent
  }
  ,
  {
    path:"products",
    component: ProductsComponent
  }
  ,
  {
    path:"users",
    component: UsersComponent
  },
  {
    path: "productDetails",
    component: ProductDetailsComponent
  },
  {
    path: "shoppingCart",
    component: ShoppingCartComponent
  },
  {
    path: "favouriteProducts",
    component: FavouriteProductsComponent
  }
  ,
  {
    path: "logout",
    component: LogoutComponent
  }

];
export const AppComponents: any = [
  HomeComponent,
  RegistrationComponent,
  LoginComponent,
  ProductsComponent,
  UsersComponent,
  ProductDetailsComponent,
  LogoutComponent

];
