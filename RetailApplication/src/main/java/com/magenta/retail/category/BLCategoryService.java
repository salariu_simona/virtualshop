package com.magenta.retail.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class BLCategoryService implements BLICategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	public void addCategory(Category category) {
		categoryRepository.save(category);
	}

	public List<Category> getAllCategories(){
		return (List<Category>) categoryRepository.findAll();
	}
}
