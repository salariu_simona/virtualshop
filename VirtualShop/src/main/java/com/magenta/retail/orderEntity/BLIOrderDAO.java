package com.magenta.retail.orderEntity;

import java.util.List;

public interface BLIOrderDAO {

	public Order findOrderById(Integer orderId);
	public List<Order> findAllOrders();
	public void deleteAllOrders();
	public void deleteOrderById(int order_id);
	public void insertOrder(Order order);
	public void updateOrder(Order order);

}