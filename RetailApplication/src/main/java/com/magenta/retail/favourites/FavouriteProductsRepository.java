package com.magenta.retail.favourites;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface FavouriteProductsRepository extends CrudRepository<FavouriteProducts, Integer> {
	public FavouriteProducts findByuserCustomerIdAndProductProductId(int user_id,int product_id);
	public List<FavouriteProducts> findByuserCustomerId(int user_id);
}