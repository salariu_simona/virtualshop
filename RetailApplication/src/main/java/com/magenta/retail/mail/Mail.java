package com.magenta.retail.mail;

import java.util.Map;

public class Mail {
	
	private String to;
	private String from;
	private String subject;
	private String content;
	private Map<?, ?> model;
	
	public Mail(String to, String from, String subject, String content) {
		super();
		this.to = to;
		this.from = from;
		this.subject = subject;
		this.content = content;
	}
	public Mail() {
		super();
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Map<?, ?> getModel() {
		return model;
	}
	public void setModel(Map<?, ?> model) {
		this.model = model;
	}
	@Override
	public String toString() {
		return "Mail [to=" + to + ", from=" + from + ", subject=" + subject + ", content=" + content + "]";
	}
	
	
	
}