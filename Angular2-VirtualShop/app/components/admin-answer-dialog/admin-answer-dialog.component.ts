import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Question } from '../../Question';
import { User } from '../../user';
import { Answer } from '../../Answer';
import { AnswerService } from '../../services/answer.service';

@Component({
  selector: 'app-admin-answer-dialog',
  templateUrl: './admin-answer-dialog.component.html',
  styleUrls: ['./admin-answer-dialog.component.css']
})
export class AdminAnswerDialogComponent implements OnInit {

  question: Question;
  answers: Answer[] = [];
  constructor(private answerService: AnswerService,public dialogRef: MatDialogRef<AdminAnswerDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: Question) {
    this.question = data;
    console.log(this.question);
   }

  ngOnInit() {
  }

  
  cancelButtonAction() {
    this.dialogRef.close();
  }

  addAnswer(content: string,questionId: number) {
    if ( content != "") {
      var answer = new Answer();
      answer.answerContent = content;
      answer.user = new User();
      answer.user.customerId = parseInt(localStorage.getItem("userId"));
      answer.question = new Question();
      answer.question.questionId = questionId;
     
      console.log(answer);
      this.answerService.addAnswer(answer).subscribe((res: Response) => {
        console.log(res);
        this.cancelButtonAction();
          

      });
    }
  }


  
}
