package com.magenta.retail.review;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BLReviewService implements BLIReviewService {

	@Autowired
	ReviewRepository reviewRepository;
	
	public void addReview(Review review) {
		reviewRepository.save(review);
	}

	public List<Review> getReviewsForProduct(int productId) {
		return reviewRepository.findByproductProductId(productId);
	}
	
}
