import { Component} from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { User, Role } from '../../user';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent  {
  private user: User;
  private errorMessage: string;
  private role:Role;

  constructor(private userService: UserService,private router:Router) { }

  registration(first_name: string, last_name: string, phone_number: string, email: string, password: string) {
    
      this.user = new User();
      this.user.firstName = first_name;
      this.user.lastName = last_name;
      this.user.phoneNumber = phone_number;
     
      this.user.email = email;
      this.user.password = password;
      this.role = new Role();
      this.role.roleId = 1;
      this.role.typeOfAccess = "user";
      this.user.role = this.role;
      console.log(this.user);
      if(this.user.email == '' || this.user.firstName == '' || this.user.lastName == '' || this.user.password == '' || this.user.phoneNumber == '') {
        this.errorMessage = 'Please complete all fields!';          
      } else {
        if( this.isValidPhoneNumber(this.user.phoneNumber) == false) {
          this.errorMessage = 'Invalid phone number!';
        } else {

          if( this.isValidEmail(this.user.email) == false) {
            this.errorMessage = 'Invalid email address!';
          } else {
            if(this.user.password.length < 8) {
              this.errorMessage = 'Password must contain at least 8 characters!';
            } else {
              this.userService.getUserByEmail(this.user).subscribe((user: string) => {
                console.log(user);
                if(user == null) {
                  console.log(this.user);
                  this.userService.putUser(this.user).subscribe((user: string) => {
                  this.router.navigate(["login"]);
                  });
                } else {
                  this.errorMessage = 'Email already exists!';
                }});
            }
          }
        }
    }
  }


  isValidPhoneNumber(phone_number) {
   return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone_number);
  }

   isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
