package com.magenta.retail.productEntity;

import java.util.List;


public interface BLIProductDAO {
	public Product findProductById(Integer product_id);
	public List<Product> findAllProducts();
	public void deleteAllProducts();
	public void deleteById(int product_id);
	public void insertProduct(Product product);
	public void updateProduct(Product product);
}
