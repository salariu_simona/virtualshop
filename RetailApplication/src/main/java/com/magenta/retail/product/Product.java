package com.magenta.retail.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magenta.retail.category.Category;
import com.magenta.retail.favourites.FavouriteProducts;
import com.magenta.retail.order.Order;
import com.magenta.retail.orderProduct.OrderProducts;
import com.magenta.retail.question.Question;
import com.magenta.retail.shoppingCart.ShoppingCart;


@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productId;
	private String name;
	private int quantity;
	private double price;
	private String photo;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "category_id")
	private Category category; 
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "PRODUCTS_ORDERS", joinColumns = @JoinColumn(name = "productId", referencedColumnName = "productId"), inverseJoinColumns = @JoinColumn(name = "orderId", referencedColumnName = "orderId"))	
	private List<Order> orders = new ArrayList<Order>();
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "product")
	private List<ShoppingCart> shoppingCartList = new ArrayList<ShoppingCart>();
	
	@OneToMany(cascade = CascadeType.MERGE,mappedBy = "product")
	private List<OrderProducts> orderProducts = new ArrayList<OrderProducts>();
	
	@OneToMany(cascade = CascadeType.MERGE,mappedBy = "product")
	private List<Question> questionList = new ArrayList<Question>();
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "product")
	private List<FavouriteProducts> favouriteProducts = new ArrayList<>();
	
	public Product() {};
	
	public int getProductId() {
		return this.productId;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	@JsonIgnore
	public List<OrderProducts> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(List<OrderProducts> orderProducts) {
		this.orderProducts = orderProducts;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	@JsonIgnore
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@JsonIgnore
	public List<ShoppingCart> getShoppingCartList() {
		return shoppingCartList;
	}

	public void setShoppingCartList(List<ShoppingCart> shoppingCartList) {
		this.shoppingCartList = shoppingCartList;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
	@JsonIgnore
	public List<FavouriteProducts> getFavouriteProducts() {
		return favouriteProducts;
	}

	public void setFavouriteProducts(List<FavouriteProducts> favouriteProducts) {
		this.favouriteProducts = favouriteProducts;
	}
	@JsonIgnore
	public List<Question> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<Question> questionList) {
		this.questionList = questionList;
	}

	public String toString() {
		String details = "{Product ID: " + this.productId + ", Name: " + this.name + ", Quantity: " + this.quantity + ", Price: " + this.price; 
		return details;	
	}
}
