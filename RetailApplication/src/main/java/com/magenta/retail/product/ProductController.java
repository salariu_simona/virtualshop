package com.magenta.retail.product;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private BLProductService product_service;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public void addProduct(@RequestBody Product Product) {
		product_service.addProduct(Product);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Product> getAllProducts() {
		return product_service.getProducts();
	}
	
	@RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
	public List<Product> getAllProductsByCategory(@PathVariable("categoryId") int categoryId) {
		return product_service.getProductsByCategory(categoryId);
	}
	
	@RequestMapping(value = "/{product_id}", method = RequestMethod.GET)
	public Product getProduct(@PathVariable("product_id") int product_id) {
		return product_service.getProductById(product_id);
	}
	
	@RequestMapping(value = "/delete/{product_id}", method = RequestMethod.DELETE)
	public void deleteProduct(@PathVariable("product_id") int product_id) {
		product_service.deleteProductById(product_id);
	}
	
	@RequestMapping(value = "/delete/all", method = RequestMethod.DELETE)
	public void deleteCustomer() {
		product_service.deleteAllProducts();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void updateProduct(@RequestBody Product product) {
		 product_service.updateProduct(product);
	}
}
