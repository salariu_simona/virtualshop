import { Category } from "./category";

export class Product {
  productId: number;
  name: string;
  price: number;
  quantity: number;
  photo: string;
  category: Category;
}
