import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { HomeComponent } from './components/home/home.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';
import { ProductsComponent } from './components/products/products.component';
import { MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogComponent } from './components/dialog/dialog.component';
import { MatFormFieldModule } from '@angular/material';
import { DialogNewProductComponent } from './components/dialog-new-product/dialog-new-product.component';
import { UsersComponent } from './components/users/users.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { DialogOrderComponent } from './components/dialog-order/dialog-order.component';
import { DialogConfirmationComponent } from './components/dialog-confirmation/dialog-confirmation.component';
import { FormsModule } from '@angular/forms';
import { FavouriteProductsComponent } from './components/favourite-products/favourite-products.component';
import { AdminAnswerDialogComponent } from './components/admin-answer-dialog/admin-answer-dialog.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    HomeComponent,
    ProductsComponent,
    DialogComponent,
    DialogNewProductComponent,
    UsersComponent,
    ProductDetailsComponent,
    ShoppingCartComponent,
    DialogOrderComponent,
    DialogConfirmationComponent,
    FavouriteProductsComponent,
    AdminAnswerDialogComponent,
    LogoutComponent
  


  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(AppRoutes),
    MatDialogModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    FormsModule




  ],
  entryComponents: [
    DialogComponent,
    DialogNewProductComponent,
    DialogOrderComponent,
    DialogConfirmationComponent,
    AdminAnswerDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
