import { Component } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { User } from '../../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
    errorMessage: string;
    flag: boolean;
    static isLogin: boolean = false;
    constructor(private userService: UserService,private router: Router) { 
      console.log( LoginComponent.isLogin);
    }

    
    loginValidation(email: string, password: string){ 
     console.log()
      this.userService.getUserByEmailAndPassword(email,password).subscribe((user: User) => {
        if(user != null) {
          this.router.navigate(["home"]);
          localStorage.setItem("typeOfAccess",user.role.typeOfAccess);
          localStorage.setItem("userId",user.customerId.toString());
          localStorage.setItem("userName",user.firstName);
         
        } else {
          this.errorMessage = 'Invalid email or password!';
          LoginComponent.isLogin = false;
        }
      });
  }
 

}
