package com.magenta.retail.customer;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer,Integer>{

	public Customer findBycustomerId(int customerId);
	public Customer findByemail(String email);
}
