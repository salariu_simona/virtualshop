package com.magenta.retail.order;

import java.util.List;

public interface BliOrderService {
	
	public Order insertOrder(Order order);
	public Order findOrderById(int id);
	public List<Order> findAllOrders();
	public Order updateOrder(Order order);
	public void deleteOrderById(int id);
	public void deleteAllOrders();
	
}
