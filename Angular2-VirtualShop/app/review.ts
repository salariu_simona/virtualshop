import { Product } from "./product";
import { User } from "./user";

export class Review {
    reviewId: number;
    product: Product;
    user: User;
    content: string;
}