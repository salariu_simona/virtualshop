package com.magenta.retail.money;
/**
 *Creating Money with quantity and value fields
 */
public class Money {
	private int quantity;
	private double value;
	
	public Money(int quantity, double d) {
		this.quantity = quantity;
		this.value = d;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
}
