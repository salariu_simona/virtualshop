package com.magenta.retail.money;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides a method for read the products from a file and add them into a list
 * 
 */

public class BLMoneyReader implements BLIMoneyReader{
	private BufferedReader br;

	public List<Money> readFile(String fileName) {
		String[] fields;
		List<Money> listOfMoney = new ArrayList<Money>();
		try {
			br = new BufferedReader(new FileReader(new File(fileName)));
			String line;
			while ((line = br.readLine()) != null) {
				fields = line.split("/");
				listOfMoney.add(new Money(Integer.parseInt(fields[0]), Double.parseDouble(fields[1])));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return listOfMoney;
	}

}
