package com.magenta.retail.orderEntity;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.magenta.retail.display.DisplayData;
import com.magenta.retail.utils.Utils;



public class BLOrderDAO implements BLIOrderDAO{
	
	private EntityManager entityManager;
	private DisplayData displayData;
	
	public BLOrderDAO() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("VirtualShop");
		entityManager = emf.createEntityManager();
		displayData = new DisplayData();
	}

	public Order findOrderById(Integer order_id) {
		displayData.printMessage(Utils.FIND_ORDER_BY_ID_MESSAGE + order_id);
		EntityTransaction findOrderByIdTransaction = entityManager.getTransaction();
		findOrderByIdTransaction.begin();
		Order order= entityManager.find(Order.class, order_id);
		findOrderByIdTransaction.commit();
		return order;
	}

	public List<Order> findAllOrders() {
		displayData.printMessage(Utils.FIND_ALL_ORDERS_MESSAGE);
		return entityManager.createQuery("SELECT o FROM Order o").getResultList();
	}

	public void deleteAllOrders() {
		displayData.printMessage(Utils.DELETE_ALL_ORDERS_MESSAGE);
		EntityTransaction deleteAllOrdersTransaction = entityManager.getTransaction();
		deleteAllOrdersTransaction.begin();
		Query deleteQuery = entityManager.createQuery("DELETE FROM Order");
		deleteQuery.executeUpdate();
		deleteAllOrdersTransaction.commit();
	}
	
	public void deleteOrderById(int order_id) {
		displayData.printMessage(Utils.DELETE_ORDER_BY_ID_MESSAGE + order_id);
		EntityTransaction deleteAllOrdersTransaction = entityManager.getTransaction();
		deleteAllOrdersTransaction.begin();
		Order order = entityManager.getReference(Order.class, order_id);
		entityManager.remove(order);
		deleteAllOrdersTransaction.commit();
	}

	public void insertOrder(Order order) {
		displayData.printMessage(Utils.INSERT_ORDER_MESSAGE);
		EntityTransaction insertOrderTransaction = entityManager.getTransaction();
		insertOrderTransaction.begin();
		entityManager.persist(order);
		insertOrderTransaction.commit();
	}

	public void updateOrder(Order order) {
		displayData.printMessage(Utils.UPDATE_ORDER_MESSAGE + order.getOrderId());
		EntityTransaction updateOrderTransaction = entityManager.getTransaction();
		updateOrderTransaction.begin();
		entityManager.merge(order);
		updateOrderTransaction.commit();
	}
}
