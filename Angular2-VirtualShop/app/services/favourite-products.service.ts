import { Injectable } from '@angular/core';
import { Http } from '@angular/http'
import { FavouriteProducts } from '../FavouriteProducts';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FavouriteProductsService {
  url: string = 'http://localhost:9000/favouriteProducts/';

  constructor(private http: Http) { }

  addToFavouriteProducts(favouriteProduct: FavouriteProducts) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.url + 'add', favouriteProduct).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
  }

  getProductsFromFavouriteProducts(userId: number) {
    return this.http.get(this.url + 'all/' + userId).pipe(map((response:Response)=>response.json()));
  }

  getProductByCustomerIdAndProductId(userId: number, productId: number) {
    return this.http.get(this.url + userId + '/' + productId).pipe(map((response:Response)=>response.json()));
  }

  deleteFromFavouriteProducts(userId: number, productId: number) {
    return this.http.delete(this.url + 'delete/' + userId + '/' + productId).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
  }

}
