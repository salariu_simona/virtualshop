package com.magenta.retail.runner;

import java.util.List;

import com.magenta.retail.addressEntity.Address;
import com.magenta.retail.addressEntity.BLAddressDAO;
import com.magenta.retail.addressEntity.BLIAddressDAO;
import com.magenta.retail.customerEntity.BLCustomerDAO;
import com.magenta.retail.customerEntity.BLICustomerDAO;
import com.magenta.retail.customerEntity.Customer;
import com.magenta.retail.display.DisplayData;
import com.magenta.retail.orderEntity.BLIOrderDAO;
import com.magenta.retail.orderEntity.BLOrderDAO;
import com.magenta.retail.orderEntity.Order;
import com.magenta.retail.productEntity.BLProductDAO;
import com.magenta.retail.productEntity.BLIProductDAO;
import com.magenta.retail.productEntity.Product;

public class RunnerDAO {

	public static void main(String args[]) {
		BLICustomerDAO customerDAO = new BLCustomerDAO();
		BLIOrderDAO orderDAO = new BLOrderDAO();
		BLIProductDAO productDAO = new BLProductDAO();
		BLIAddressDAO addressDAO = new BLAddressDAO();
		DisplayData displayData = new DisplayData();
		
		// OPERATIONS FOR ADDRESS TABLE
		
		Address address_1 = new Address();
		address_1.setStreet_name("Primaverii");
		address_1.setNumber("237");
		address_1.setCity("Iasi");
		
		Address address_2 = new Address();
		address_2.setStreet_name("Mihai Eminescu");
		address_2.setNumber("153");
		address_2.setCity("Iasi");
		
		addressDAO.insertAddress(address_1); 
		addressDAO.insertAddress(address_2);
		
		List<Address> listOfAddress = addressDAO.findAllAddresses();
	    displayData.showAddress("Address List: ",listOfAddress);
	    
		Address found_address = addressDAO.findAddressById(3);
		displayData.printMessage("Address found: "+ found_address.toString());
		
		address_1.setCity("Bucuresti");
		addressDAO.updateAddress(address_1);	
	    displayData.printMessage("After update: "+ address_1.toString());
	    
	    addressDAO.deleteAddressById(4);
	    listOfAddress = addressDAO.findAllAddresses();
	    displayData.showAddress("After delete: ",listOfAddress);
		
		// OPERATIONS FOR CUSTOMER TABLE
		
		Customer customer_1 = new Customer();
		customer_1.setFirst_name("Ioana");
		customer_1.setLast_name("Popescu");
		customer_1.setPhone_number("0742849555");
		customer_1.setEmail("ioana_popescu@yahoo.com");
		customer_1.addCostumerAddress(address_1);
		customer_1.addCostumerAddress(address_2);
		customerDAO.insertCustomer(customer_1);
		
		List<Customer> listOfCustomers = customerDAO.findAllCustomers();
	    displayData.showCustomers("List of Customers:  ",listOfCustomers);
	    
		Customer customer_found = customerDAO.findCustomerById(4);
		displayData.printMessage("Customer found:" + customer_found.toString());
		
		customer_1.setFirst_name("Ioana-Maria");
		customerDAO.updateCustomer(customer_1);
		displayData.printMessage("Customer update:" + customer_1.toString());
		
		customerDAO.deleteCustomerById(3);
		
		listOfCustomers = customerDAO.findAllCustomers();
	    displayData.showCustomers("List after delete:  ",listOfCustomers);
		
		
		// OPERATIONS FOR ORDER TABLE
		
		Order order_1 = new Order();
		order_1.setFinalPrice(5200.0);
		order_1.setQuantity(1);
		order_1.setPayment_method("LEI");
		order_1.setCustomer(customer_1);
		
		Order order_2 = new Order();
		order_2.setFinalPrice(10000.0);
		order_2.setQuantity(2);
		order_2.setPayment_method("LEI");
		order_2.setCustomer(customerDAO.findCustomerById(4));
		
		orderDAO.insertOrder(order_1);
		orderDAO.insertOrder(order_2);
		
		List<Order> listOfOrders= orderDAO.findAllOrders();
	    displayData.showOrders("List of Orders: ",listOfOrders);
	    
		order_2.setPayment_method("EURO");
		orderDAO.updateOrder(order_2);
		
		Order order_found = orderDAO.findOrderById(2);
		displayData.printMessage("Order found: " + order_found.toString());
		
		orderDAO.deleteOrderById(2);
		 listOfOrders= orderDAO.findAllOrders();
	    displayData.showOrders("Orders after delete: ",listOfOrders);
	    
		
		// OPERATIONS FOR TABLE PRODUCT
		
		Product product_1 = new Product();
		product_1.setName("iPhone X");
		product_1.setPrice(5200);
		product_1.setQuantity(10);
		product_1.addOrders(order_1);
		
		Product product_2 = new Product();
		product_2.setName("Samsung Galaxy S9");
		product_2.setPrice(3000);
		product_2.setQuantity(10);
		
		productDAO.insertProduct(product_1);
		productDAO.insertProduct(product_2);
		

	    List<Product> listOfProducts= productDAO.findAllProducts();
	    displayData.showProducts("List of Products:",listOfProducts);
	    
		Product product_found = productDAO.findProductById(1);
		displayData.printMessage("Product found: " + product_found.toString());
		
		product_found.setQuantity(30);
		productDAO.updateProduct(product_found);
		
		displayData.printMessage("Product update: " + product_found.toString());
		
		productDAO.deleteById(4);
		listOfProducts= productDAO.findAllProducts();
		displayData.showProducts("List after delete: ",listOfProducts);
		
		
		customerDAO.deleteAllCustomers();
	    addressDAO.deleteAllAddresses();
	    productDAO.deleteAllProducts();
	    orderDAO.deleteAllOrders();

	}
}
