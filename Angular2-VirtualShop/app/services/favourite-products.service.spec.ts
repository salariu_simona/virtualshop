import { TestBed, inject } from '@angular/core/testing';

import { FavouriteProductsService } from './favourite-products.service';

describe('FavouriteProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FavouriteProductsService]
    });
  });

  it('should be created', inject([FavouriteProductsService], (service: FavouriteProductsService) => {
    expect(service).toBeTruthy();
  }));
});
