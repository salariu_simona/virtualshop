package com.magenta.retail.productEntity;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.magenta.retail.display.DisplayData;
import com.magenta.retail.utils.Utils;


public class BLProductDAO implements BLIProductDAO{

	private EntityManager entityManager;
	private DisplayData displayData;
	public BLProductDAO() {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("VirtualShop");
		entityManager = entityManagerFactory.createEntityManager();
		displayData = new DisplayData();
	}
	public Product findProductById(Integer product_id) {
		displayData.printMessage(Utils.FIND_PRODUCT_BY_ID_MESSAGE + product_id);
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		Product product = entityManager.find(Product.class, product_id);
		transaction.commit();
		return product;
	}

	public List<Product> findAllProducts() {
		displayData.printMessage(Utils.FIND_ALL_PRODUCTS_MESSAGE);
		return entityManager.createQuery("SELECT p FROM Product p").getResultList();
	}

	public void deleteAllProducts() {
		displayData.printMessage(Utils.DELETE_ALL_PRODUCTS_MESSAGE);
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		Query query = entityManager.createNativeQuery("DELETE FROM product");
		query.executeUpdate();
		transaction.commit();	
	}

	public void insertProduct(Product product) {
		displayData.printMessage(Utils.INSERT_PRODUCT_MESSAGE);
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.persist(product);
		transaction.commit();
		
	}
	public void deleteById(int product_id) {
		displayData.printMessage(Utils.DELETE_PRODUCT_BY_ID_MESSAGE + product_id);
		EntityTransaction deleteAllProductsTransaction = entityManager.getTransaction();
		deleteAllProductsTransaction.begin();
		Product product = entityManager.getReference(Product.class, product_id);
		entityManager.remove(product);
		deleteAllProductsTransaction.commit();
	}

	public void updateProduct(Product product) {
		displayData.printMessage(Utils.UPDATE_PRODUCT_MESSAGE + product.getProduct_id());
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.merge(product);
		transaction.commit();
	}
}
