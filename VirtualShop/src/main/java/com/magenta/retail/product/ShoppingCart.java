package com.magenta.retail.product;

import java.util.HashMap;
import java.util.Map;

/** 
 * This class provides all necesarry method for shopping cart management
 *
 */
public class ShoppingCart {

	private Map<Integer, Product> shoppingCart = new HashMap<Integer, Product>();
	private double totalPrice = 0.0;

	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double price) {
		totalPrice = price;
	}

	public Map<Integer, Product> getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(Map<Integer, Product> shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	// add product to shopping cart
	public void addToShoppingCart(ProductStock productStock, int productIndex, int numberOfProducts) {
		Product newProduct = new Product.ProductBuilder(productStock.getStockOfProducts().get(productIndex).getName())
				.quantity(numberOfProducts).price(productStock.getStockOfProducts().get(productIndex).getPrice())
				.build();
		shoppingCart.put(productIndex, newProduct);
	}

	// method for returning price for all products from shopping cart
	public ShoppingCart totalPriceCalculation() {
		for (Map.Entry<Integer, Product> product : shoppingCart.entrySet()) {
			totalPrice += product.getValue().getQuantity() * product.getValue().getPrice();
		}
		return this;
	}
	
	// delete shopping cart for current client when the tranzaction is finished
	public void deleteProductsFromCart() {
		shoppingCart.clear();	
	}
}
