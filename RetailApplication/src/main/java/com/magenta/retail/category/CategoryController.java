package com.magenta.retail.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {


	@Autowired
	private BLCategoryService category_service;

	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	private void addCategory(@RequestBody Category category) {
		category_service.addCategory(category);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	private List<Category> getCategories() {
		return category_service.getAllCategories();
	}
}
