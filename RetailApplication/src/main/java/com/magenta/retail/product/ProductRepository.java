package com.magenta.retail.product;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface ProductRepository extends CrudRepository<Product,Integer>{
	public Product getByProductId(int productId);
	public List<Product> getBycategoryCategoryId(int categoryId);
}
