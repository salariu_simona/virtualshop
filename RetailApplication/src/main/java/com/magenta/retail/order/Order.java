package com.magenta.retail.order;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magenta.retail.customer.Customer;
import com.magenta.retail.orderProduct.OrderProducts;
import com.magenta.retail.product.Product;

@Entity(name = "Orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int orderId;
	private double finalPrice;
	private String paymentMethod;
	private String orderDate;
	private String deliveryAddress;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "user_id")
	private Customer user;
	

	@ManyToMany(mappedBy = "orders")
	private List<Product> products = new ArrayList<Product>();

	@OneToMany(cascade = CascadeType.MERGE,mappedBy = "order")
	private List<OrderProducts> orderProducts = new ArrayList<OrderProducts>();

	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	@JsonIgnore
	public List<OrderProducts> getOrderProducts() {
		return orderProducts;
	}
	public void setOrderProducts(List<OrderProducts> orderProducts) {
		this.orderProducts = orderProducts;
	}
	public Order() {};
	public int getOrderId() {
		return orderId;
	}
	
	
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public double getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

   
	public Customer getUser() {
		return user;
	}
	public void setUser(Customer user) {
		this.user = user;
	}
	@JsonIgnore
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	@Override
	public String toString() {
		String orderDetails = "{Order ID:" + this.orderId + "final price=" + finalPrice + ", payment method="
				+ paymentMethod + ", customer: " + user + "}";
		return orderDetails;
	}
}