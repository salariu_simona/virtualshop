package com.magenta.retail.productEntity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.magenta.retail.orderEntity.Order;


@Entity
@Table
@CascadeOnDelete
@XmlRootElement
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int product_id;
	private String name;
	private int quantity;
	private double price;
	private String photo;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "PRODUCTS_ORDERS", joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "product_id"), inverseJoinColumns = @JoinColumn(name = "orderId", referencedColumnName = "orderId"))
	@CascadeOnDelete
	private List<Order> orders = new ArrayList<Order>();
	
	
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	public void addOrders(Order order) {
		orders.add(order);
		}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String toString() {
		String details = "{Product ID: " + this.product_id + ", Name: " + this.name + ", Quantity: " + this.quantity + ", Price: " + this.price; 
		if(orders.size() != 0) {
			details += ", Orders: " + orders + "}";
		} else {
			details += "}";
		}
		return details;	
	}
}
