package com.magenta.retail.money;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class MoneyStockTest {
	MoneyStock moneyStock = new MoneyStock();
	@Before
	void init() {
		Money bill1 = new Money(5,500);
		Money bill2 = new Money(5,200);
		Money bill3 = new Money(5,100);
		Money bill4 = new Money(5,50);
		Money bill5 = new Money(5,10);
		Money bill6 = new Money(5,5);
		Money bill7 = new Money(5,1);
		
		Money coin1 = new Money(5,0.5);
		Money coin2 = new Money(5,0.1);
		Money coin3 = new Money(5,0.05);
		Money coin4 = new Money(5,0.01);
		
		moneyStock.getStockOfMoney().put(1, bill1);
		moneyStock.getStockOfMoney().put(2, bill2);
		moneyStock.getStockOfMoney().put(3, bill3);
		moneyStock.getStockOfMoney().put(4, bill4);
		moneyStock.getStockOfMoney().put(51, bill5);
		moneyStock.getStockOfMoney().put(6, bill6);
		moneyStock.getStockOfMoney().put(7, bill7);
		moneyStock.getStockOfMoney().put(8, coin1);
		moneyStock.getStockOfMoney().put(9, coin2);
		moneyStock.getStockOfMoney().put(10, coin3);
		moneyStock.getStockOfMoney().put(11, coin4);
	
	}
	

}
