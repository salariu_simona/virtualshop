package com.magenta.retail.shoppingCart;

import java.util.List;

public interface BLIShoppingCartService {

	public void addToShoppingCart(ShoppingCart shoppingCart);
	
	public void deleteShoppingCart(int user_id);
	
	public List<ShoppingCart> getShoppingCart(int user_id);
	
	public void deleteProductFromShoppingCart(int user_id,int product_id) ;
}
