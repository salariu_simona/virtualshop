package com.magenta.retail.customer;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private BLCustomerService customer_service;

	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public void addCustomer(@RequestBody Customer customer) {
		customer_service.addCustomer(customer);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@JsonIgnore
	public List<Customer> getAllCustomers() {
		return customer_service.getCustomers();
	}

	@RequestMapping(value = "/{customer_id}", method = RequestMethod.GET)
	public Customer getCustomer(@PathVariable("customer_id") int customer_id) {
		return customer_service.getCustomerById(customer_id);
	}
	
	@RequestMapping(value = "/getUser", method = RequestMethod.POST)
	public Customer getCustomerByEmail(@RequestBody Customer customer) {
		return customer_service.getCustomerByEmail(customer.getEmail());
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Customer loginValidation(@RequestBody Customer customer) {
		return customer_service.getCustomerByEmailAndPassword(customer);
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public Customer registerValidation(@RequestBody Customer customer) {
		return  customer_service.getCustomerByEmail(customer.getEmail());
		
	}

	@RequestMapping(value = "/delete/{customer_id}", method = RequestMethod.DELETE)
	public void deleteCustomer(@PathVariable("customer_id") int customer_id) {
		customer_service.deleteCustomerById(customer_id);
	}

	@RequestMapping(value = "/delete/all", method = RequestMethod.DELETE)
	public void deleteCustomers() {
		customer_service.deleteAllCustomers();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void updateCustomer(@RequestBody Customer customer) {
		customer_service.updateCustomer(customer);
	}
}
