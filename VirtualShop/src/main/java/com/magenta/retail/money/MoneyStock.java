package com.magenta.retail.money;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.magenta.retail.utils.Utils;

/**
 * This class provides a list of money which means the store's budge, read from a file
 */
public class MoneyStock {
	private Map<Integer, Money> stockOfMoney;

	public MoneyStock() {
		stockOfMoney = new HashMap<Integer, Money>();
	}
	public Map<Integer, Money> getStockOfMoney() {
		return stockOfMoney;
	}

	public void setStockOfMoney(Map<Integer, Money> stockOfMoney) {
		this.stockOfMoney = stockOfMoney;
	}

	// method for initialize the stock of money
	public void initializeStockOfMoney() {
		BLIMoneyReader reader = new BLMoneyReader();
		List<Money> listOfMoney = new ArrayList<Money>();
		listOfMoney = reader.readFile(Utils.MONEY_FILE_PATH);
		for (int i = 0; i < listOfMoney.size(); i++) {
			stockOfMoney.put(i + 1, listOfMoney.get(i));
		}
	}

	// return the current sum which client choose to pay 
	public double getCurrentSum(int moneyIndex, int numberOfCoinsOrBills) {
		return numberOfCoinsOrBills * stockOfMoney.get(moneyIndex).getValue();
	}

	// update the stock of money when the client paid the command
	public void addMoneyToStock(int moneyIndex, int numberOfCoinsOrBills) {
		int currentQuantity = stockOfMoney.get(moneyIndex).getQuantity();
		stockOfMoney.get(moneyIndex).setQuantity(currentQuantity + numberOfCoinsOrBills);
	}

	// remove money from stock when a command is canceled
	public void removeMoneyFromStock(int moneyIndex, int numberOfCoinsOrBills) {
		int currentQuantity = stockOfMoney.get(moneyIndex).getQuantity();
		stockOfMoney.get(moneyIndex).setQuantity(currentQuantity - numberOfCoinsOrBills);
	}

	// make a copy of a stock for using when a command is canceled
	public List<Money> getCopyStockOfMoney() {
		List<Money> list = new ArrayList<Money>();
		for (Map.Entry<Integer, Money> money : stockOfMoney.entrySet()) {
			Money newMoney = new Money(money.getValue().getQuantity(),money.getValue().getValue());
			list.add(newMoney);
		}
		return list;
	}
	
	// update the stock of money when the client pay for his command
	public void updateStockOfMoney(List<Money> copyList) {
		for (int i = 0; i < stockOfMoney.size(); i++) {
			stockOfMoney.put(i + 1, copyList.get(i));
		}
	}

}
