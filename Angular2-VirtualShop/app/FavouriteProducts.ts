import { Product } from "./product";
import { User } from "./user";

export class FavouriteProducts {
    favouriteProductId: number;
    product: Product;
    user: User;
}