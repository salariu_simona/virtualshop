package com.magenta.retail.address;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlAddressService implements BliAddressService {
	
	@Autowired
	private AddressRepository addressRepository;

	@Override
	public Address insertAddress(Address address) {
		return addressRepository.save(address);
	}
	
	@Override
	public Address findAddressById(int id) {
		return addressRepository.getByaddressId(id);
	}

	@Override
	public List<Address> findAllAddresses() {
		return (List<Address>)addressRepository.findAll();
	}
	
	@Override
	public Address updateAddress(Address address) {
		Address updateAddress = addressRepository.getByaddressId(address.getAddressId());
		updateAddress.setCity(address.getCity());
		updateAddress.setCountry(address.getCountry());
		updateAddress.setNumber(address.getNumber());
		updateAddress.setStreet_name(address.getStreet_name());
		return addressRepository.save(updateAddress);
	}

	@Override
	public void deleteAllAddresses() {
		addressRepository.deleteAll();
	}

	@Override
	public void deleteAddressById(int id) {
		addressRepository.deleteById(id);
	}

}
