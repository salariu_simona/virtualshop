import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Order, OrderProduct } from '../Order';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  url: string = 'http://localhost:9000/order';
  

  constructor(private http: Http) { }

  public insertOrder(order: Order) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.put(this.url + '/add',order).pipe(map((response: Response)=>response.json()));
   }

   public insertOrderProduct(orderProducts : OrderProduct) {
    return this.http.put(this.url + 'Product/add',orderProducts).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json())); 


    
   }
}
