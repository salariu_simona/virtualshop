package com.magenta.retail.address;

import java.util.List;

import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class AddressController {
	
	@Autowired
	BliAddressService addressService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public Address addAddress(@RequestBody Address address) {
		return addressService.insertAddress(address);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Address getById(@PathVariable("id") @NotNull int id) {
		return addressService.findAddressById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Address> getAddresses() {
		return addressService.findAllAddresses();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Address updateAddress(@RequestBody Address address) {
		return addressService.updateAddress(address);
	}
	
	@RequestMapping(value = "/delete/all", method = RequestMethod.DELETE)
	public void deleteAllAddresses() {
		addressService.deleteAllAddresses();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteAddressById(@PathVariable @NotNull int id) {
		addressService.deleteAddressById(id);
	}
}
