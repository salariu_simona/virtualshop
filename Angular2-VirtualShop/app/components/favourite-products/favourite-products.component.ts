import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FavouriteProducts } from '../../FavouriteProducts';
import { FavouriteProductsService } from '../../services/favourite-products.service';

@Component({
  selector: 'app-favourite-products',
  templateUrl: './favourite-products.component.html',
  styleUrls: ['./favourite-products.component.css']
})
export class FavouriteProductsComponent implements OnInit {
  favouriteProducts: FavouriteProducts[];

  constructor(private router: Router, private favouriteProductsService: FavouriteProductsService) { }

  ngOnInit() {
    this.getFavouriteProducts();
  }

  getFavouriteProducts() {
    let user = parseInt(localStorage.getItem("userId"));
    this.favouriteProductsService.getProductsFromFavouriteProducts(user)
      .subscribe((data: FavouriteProducts[]) => {
        this.favouriteProducts = data;
        console.log(this.favouriteProducts);
      });
  }

  seeProduct(productId: number) {
    localStorage.setItem("id", productId.toString());
    this.router.navigate(["productDetails"]);
  }

  deleteProductFromFavouriteProducts(productId: number) {
    let userId = parseInt(localStorage.getItem("userId"));
    this.favouriteProductsService.deleteFromFavouriteProducts(userId, productId)
      .subscribe((res: Response) => {
        location.reload();
      });
  }
}
