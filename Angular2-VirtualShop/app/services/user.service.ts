import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } 
from '@angular/http';
import { map } from 'rxjs/operators';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
    url: string = 'http://localhost:9000/customer/';
  
  constructor(private http: Http) {
      console.log("User service is initialized..");
   }

   getUserByEmailAndPassword(email: string, password: string) {
     
      let user = new User();
      user.email = email;
      user.password = password;
     
     return this.http.post(this.url + 'login',user,{})
     .pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }
    getUserByEmail(user: User) {
     console.log('getUserByEmail email: ' + user.email);
    return this.http.post(this.url + 'registration',user,{}).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }
   putUser(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.url + 'add',user).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }
   getUser(user: User) {
     return this.http.post(this.url + 'getUser',user).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
   }
   findAllUsers() {
    return this.http.get(this.url +'all')
              .pipe(map((response : Response) => <User[]>response.json()));
  }   

  updateUser(user: User) {
    return this.http.post(this.url + 'update',user,{}).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json())); 
  }
}
