import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Product } from '../../product';
import { ProductService } from '../../services/product.service';
import { Category } from '../../category';
import { CategoryService } from '../../services/category.service';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  public product: Product;
  public categories:Category[];
  categorySelected: string;
  categoryId: number;
 


  constructor(private categoryService: CategoryService,private productService: ProductService,public dialogRef : MatDialogRef<DialogComponent>,@Inject(MAT_DIALOG_DATA) public data: Product){
  this.product = data;
 }

 ngOnInit() {
   this.getCategories();

  }
  cancelButtonAction() {
    this.dialogRef.close();
    
  }
  updateButtonAction(product: Product, name: string,price: number, quantity: number,photo: string) {
    if(name != "") {
      product.name = name;
    }
    if(price > 0) {
      product.price = price;
    }
    if(quantity >= 0 && quantity != 0) {
      product.quantity = quantity;
    }
    if(photo != ""){
      product.photo = photo;
    }

      product.category = new Category();
      product.category.categoryId = this.categoryId;
      product.category.categoryName = this.categorySelected;
    
      console.log(this.categoryId);
      console.log(this.categorySelected);
      console.log(product);
      this.productService.updateProduct(product).subscribe((res: Response) => { console.log(res); });
    
   
    this.cancelButtonAction();
  }

  getCategories() {
    this.categoryService.findAllCategories().subscribe((data: Category[]) => { this.categories = data;
      console.log(this.categories); 
      this.categorySelected = this.categories[0].categoryName;
      this.categoryId = this.categories[0].categoryId;
      
    });
  }

  onChange(deviceValue) {
    this.categorySelected = deviceValue;
    this.categories.forEach(category => {
      if(category.categoryName == deviceValue) {
         this.categoryId = category.categoryId;
         this.categorySelected = category.categoryName;
      }  
    });
  }
}
