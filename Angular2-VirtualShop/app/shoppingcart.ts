import { Product } from "./product";
import { User } from "./user";

export class ShoppingCart {
    shoppingId: number;
    product: Product;
    user: User;
    quantity: number;

}

export class ProductQuantityPair {
    product: Product;
    quantity: number;
}