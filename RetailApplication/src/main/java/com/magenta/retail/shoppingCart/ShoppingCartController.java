package com.magenta.retail.shoppingCart;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController	
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
	
	@Autowired
	private BLShoppingCartService shoppingCartService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	private void addShoppingCart(@RequestBody ShoppingCart shoppingCart) {
		shoppingCartService.addToShoppingCart(shoppingCart);
	}

	@RequestMapping(value = "/all/{user_id}", method = RequestMethod.GET)
	private List<ShoppingCart> getShoppingCart(@PathVariable("user_id") int user_id) {
		return shoppingCartService.getShoppingCart(user_id);
	}
	
	@RequestMapping(value = "/delete/{user_id}", method = RequestMethod.DELETE)
	private void deleteShoppingCart(@PathVariable int user_id) {
		shoppingCartService.deleteShoppingCart(user_id);
	}
	
	@RequestMapping(value = "/delete/{user_id}/{product_id}", method = RequestMethod.DELETE)
	private void deleteShoppingCart(@PathVariable("user_id") int user_id, @PathVariable("product_id") int product_id) {
		shoppingCartService.deleteProductFromShoppingCart(user_id, product_id);
	}
}
