package com.magenta.retail.address;

import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Integer>{

	Address getByaddressId(int id);

}