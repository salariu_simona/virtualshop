package com.magenta.retail.address;

import java.util.List;


public interface BliAddressService {
	
	public Address insertAddress(Address address);
	public Address findAddressById(int id);
	public List<Address> findAllAddresses();
	public Address updateAddress(Address address);
	public void deleteAddressById(int id);
	public void deleteAllAddresses();
	
}
