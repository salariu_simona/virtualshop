package com.magenta.retail.shoppingCart;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BLShoppingCartService implements BLIShoppingCartService {

	@Autowired
	ShoppingCartRepository shoppingCartRepository;
	public void addToShoppingCart(ShoppingCart shoppingCart) {
		ShoppingCart existingShoppingCart = shoppingCartRepository.findByuserCustomerIdAndProductProductId(shoppingCart.getUser().getCustomerId(), shoppingCart.getProduct().getProductId());
		if(existingShoppingCart == null) {
			shoppingCartRepository.save(shoppingCart);
		} else {
			existingShoppingCart.setQuantity(existingShoppingCart.getQuantity() + shoppingCart.getQuantity());
			shoppingCartRepository.save(existingShoppingCart);
		}
	}
	public void deleteShoppingCart(int user_id) {
		List<ShoppingCart> shoppingList = shoppingCartRepository.findByuserCustomerId(user_id);
		for(int i = 0 ; i < shoppingList.size(); i++) {
			shoppingCartRepository.delete(shoppingList.get(i));
		}
		
	}
	
	public void deleteProductFromShoppingCart(int user_id,int product_id) {
		ShoppingCart shoppingCart = shoppingCartRepository.findByuserCustomerIdAndProductProductId(user_id, product_id);
		shoppingCartRepository.delete(shoppingCart);
	}

	public List<ShoppingCart> getShoppingCart(int user_id) {
		return shoppingCartRepository.findByuserCustomerId(user_id);
		
	}
	
	


}
