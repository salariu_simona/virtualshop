package com.magenta.retail.orderEntity;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/order")
public class OrderService {

	private BLOrderDAO orderDAO = new BLOrderDAO();

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Order> getAllCustomers() {
		return orderDAO.findAllOrders();
	}

	@GET
	@Path("/{order_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Order getCustomerById(@PathParam("customer_id") int customer_id) {
		return orderDAO.findOrderById(customer_id);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public void insertCustomer(Order order) {
		orderDAO.insertOrder(order);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void updateOrder(Order order) {
		orderDAO.updateOrder(order);
	}

	@DELETE
	@Path("/{order_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteOrder(@PathParam("order_id") int order_id) {
		orderDAO.deleteOrderById(order_id);
	}

	@DELETE
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteOrders() {
		orderDAO.deleteAllOrders();
	}
}