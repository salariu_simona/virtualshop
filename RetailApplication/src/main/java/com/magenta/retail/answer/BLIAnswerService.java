package com.magenta.retail.answer;

import java.util.List;

public interface BLIAnswerService {

	public void addAnswer(Answer answer);
	
	public List<Answer> getAnswersForQuestion(int questionId);
}
