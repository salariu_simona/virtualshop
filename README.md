# VirtualShop
Virtual Shop reprezintă o aplicație ce are scopul de a simula un proces de achiziționare a mai multor produse de același tip sau de tipuri diferite.
Interacțiunea dintre utilizator și aplicație se realizează prin intermediul consolei. 

1. Acestuia i se prezintă o listă de produse,din care poate alege unul sau mai multe in funcție de necesități și de stocul disponibil

2. Clientului i se oferă posibilitatea de a adaugare a mai multor produse în coș și, de asemenea, de a finaliza comanda 

3. In urma finalizării comenzii i se afișează suma totală de plată, după care este întrebat despre modalitatea prin care dorește să facă plata: banconte sau monede 

4. Următorul pas este să introducă numărul de bancnote/monede, scăzându-i-se din suma totală suma curentă plătită

5. Se repetă pasul anterior până când clientul a achitat suma totală

6. Aplicația se reia de la început pentru următorul client

