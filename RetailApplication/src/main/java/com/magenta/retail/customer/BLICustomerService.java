package com.magenta.retail.customer;

import java.util.List;

public interface BLICustomerService {

	public void addCustomer(Customer customer);
	
	public List<Customer> getCustomers();
	
	public Customer getCustomerById(int customer_id);
	
	public void deleteCustomerById(int customer_id);
	
	public void deleteAllCustomers();
	
	public void updateCustomer(Customer customer);
	
	public Customer getCustomerByEmailAndPassword(Customer customer) ;
	
	public Customer getCustomerByEmail(String email);

}

