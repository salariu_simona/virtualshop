import { Component, OnInit } from '@angular/core';
import { Product } from "../../product";
import { ProductService } from '../../services/product.service';
import { ShoppingCart } from '../../shoppingcart';
import { User } from '../../user';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { DialogConfirmationComponent } from '../dialog-confirmation/dialog-confirmation.component';
import { MatDialog } from '@angular/material';
import { Router, NavigationEnd } from '@angular/router';
import { Review } from '../../review';
import { ReviewService } from '../../services/review.service';
import { FavouriteProductsService } from '../../services/favourite-products.service';
import { FavouriteProducts } from '../../FavouriteProducts';
import { Question } from '../../Question';
import { QuestionService } from '../../services/question.service';
import { AdminAnswerDialogComponent } from '../admin-answer-dialog/admin-answer-dialog.component';
import { Answer } from '../../Answer';
import { AnswerService } from '../../services/answer.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
 
  private product: Product;
  stockMessage: string;
  className: string;
  numbers: number[];
  shoppingCart: ShoppingCart;
  isFavourited: Boolean;
  favouriteProduct: FavouriteProducts;
  selectedQuantity: number = 1;
  productAdded: string;
  private timer;
  sectionScroll: string;
  reviews: Review[] = [];
  questions: Question[] = [];
  defaultValue1: string;
  defaultValue2: string;
  answers: Answer[] = [];
  noAnswers: string;
  currentQuestionId: number;

  constructor(private answerService: AnswerService,private questionService:QuestionService,private favouriteProductsService: FavouriteProductsService, private reviewService: ReviewService, private router: Router, private productService: ProductService, private shopService: ShoppingCartService, private dialog: MatDialog) { 
  
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.doScroll();
      this.sectionScroll = null;
    });

    this.productService.getProductById(parseInt(localStorage.getItem("id")))
      .subscribe((data: Product) => {
        this.product = data;
        this.getProductByCustomerId(this.product.productId);
        this.getReviewForProduct();
        this.getQuestionForProduct();
  
        if (data.quantity > 10) {
          this.numbers = Array.from(Array(10), (x, i) => i + 1);
        } else {
          this.numbers = Array.from(Array(data.quantity), (x, i) => i + 1);
        }
        this.setStockMessageAndClassName();
      });


  }

  setStockMessageAndClassName() {
    if (this.product.quantity == 0) {
      this.stockMessage = 'out of stock';
      this.className = 'outOfStock';
    } else if (this.product.quantity <= 2) {
      this.stockMessage = 'only few products in stock';
      this.className = 'onlyFew';
    } else {
      this.stockMessage = 'in stock';
      this.className = 'inStock';
    }
  }
  addToCart(product: Product) {
    this.shoppingCart = new ShoppingCart();
    this.shoppingCart.product = new Product();
    this.shoppingCart.product.productId = product.productId;
    this.shoppingCart.user = new User();
    this.shoppingCart.user.customerId = parseInt(localStorage.getItem("userId"));

    this.shoppingCart.quantity = this.selectedQuantity;
    this.shopService.addToShoppingCart(this.shoppingCart).subscribe((shoppingCart: ShoppingCart) => { console.log(shoppingCart); });
    this.openDialog();

  }

  onChange(deviceValue) {
    this.selectedQuantity = deviceValue;
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '200px',
      height: '90px',
    });
    this.timer = setTimeout(() => dialogRef.close(), 500);
  }

  internalRoute(page, dst) {

    this.sectionScroll = dst;
    this.router.navigate([page], { fragment: dst });

  }
  doScroll() {

    if (!this.sectionScroll) {
      return;
    }
    try {
      var elements = document.getElementById(this.sectionScroll);

      elements.scrollIntoView();
    }
    finally {
      this.sectionScroll = null;
    }
  }

  getReviewForProduct() {
    this.reviewService.getReview(this.product.productId).subscribe((data: Review[]) => {
      this.reviews = data;
      console.log(data);
    })
  }

  getQuestionForProduct() {
    this.questionService.getQuestion(this.product.productId).subscribe((data: Question[]) => {
      this.questions = data;
      console.log(data);
    
     
    })
  }

  addReview(content: string) {
    if (content != "") {
      var review = new Review();
      review.content = content;
      review.user = new User();
      review.user.customerId = parseInt(localStorage.getItem("userId"));
      review.product = new Product();
      review.product = this.product

      this.reviewService.addReview(review).subscribe((res: Response) => {
        console.log(res);
        this.getReviewForProduct();
        this.defaultValue1 = "";

      });
    }
  }
  addQuestion(content: string) {
    if (content != "") {
      var question = new Question();
      question.questionContent = content;
      question.user = new User();
      question.user.customerId = parseInt(localStorage.getItem("userId"));
      question.product = new Product();
      question.product = this.product

      this.questionService.addQuestion(question).subscribe((res: Response) => {
        console.log(res);
        this.getQuestionForProduct();
        this.defaultValue2 = "";

      });
    }
  }

  getProductByCustomerId(productId) {
    let userId = parseInt(localStorage.getItem("userId"));
    this.favouriteProductsService.getProductByCustomerIdAndProductId(userId, productId)
      .subscribe((data: boolean) => {
        this.isFavourited = data
        console.log(data);
      });
  }

  addToFavouriteProducts(productId: number) {
    this.favouriteProduct = new FavouriteProducts();
    this.favouriteProduct.product = new Product();
    this.favouriteProduct.product.productId = productId;
    this.favouriteProduct.user = new User();
    this.favouriteProduct.user.customerId = parseInt(localStorage.getItem("userId"));
    this.favouriteProductsService.addToFavouriteProducts(this.favouriteProduct)
      .subscribe((favouriteProduct: FavouriteProducts) => {
        console.log(favouriteProduct);
        location.reload();
      });
  }

  deleteFromFavouriteProducts(productId: number) {
    let userId = parseInt(localStorage.getItem("userId"));
    this.favouriteProductsService.deleteFromFavouriteProducts(userId, productId)
      .subscribe((res: Response) => {
        location.reload();
      });
  }

  getTypeOfAccess(){
    return localStorage.getItem("typeOfAccess");
  }
  addAnswer(question:Question) {
    const dialogRef = this.dialog.open(AdminAnswerDialogComponent, {
      width: '440px',
      height: '320px',
      data: {
        q : question
      }
    });
  }
  
    
  
  viewAnswers(questionId: number) {
    this.answers = [];
      this.answerService.getAnswers(questionId).subscribe((data: Answer[]) => {
        this.answers = data;
        this.noAnswers = "";
      if(data.length == 0) {
        this.noAnswers = "No answers yet!";
        this.currentQuestionId = questionId;
      } 
      
    });
    
  }

}
