
export class User {
    customerId: number;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    password: string;
    role: Role
  }

  export class Role {
    roleId: number;
    typeOfAccess: string;
  }