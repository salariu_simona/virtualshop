package com.magenta.retail.roles;

import org.springframework.data.repository.CrudRepository;

public interface RolesRepository extends CrudRepository<Roles, Integer> {
	public Roles getByroleId(int roleId);
}