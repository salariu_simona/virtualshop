import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Product } from '../product';
import { map } from 'rxjs/operators';
import { ShoppingCart } from '../shoppingcart';


@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  url: string = 'http://localhost:9000/shoppingCart/';

  constructor(private http: Http) { }

  addToShoppingCart(shoppingCart: ShoppingCart) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.url + 'add',shoppingCart).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
  }

  getProductsFromShoppingCart(userId:number) {
    return this.http.get(this.url + 'all/' + userId).pipe(map((response:Response)=>response.json()));
  }

  deleteFromShoppingCart(userId: number,productId: number) {
    return this.http.delete(this.url + 'delete/' + userId + '/' + productId).pipe(map((res : Response) => (<any>res)._body == '' ? null : res.json()));
  }
}
