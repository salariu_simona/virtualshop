package com.magenta.retail.productEntity;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/product")
public class ProductService {

	private BLProductDAO productDAO = new BLProductDAO();

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllProducts() {
		return productDAO.findAllProducts();
	}

	@GET
	@Path("/{product_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProductById(@PathParam("product_id") int product_id) {
		return productDAO.findProductById(product_id);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public void insertProduct(Product order) {
		productDAO.insertProduct(order);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void updateProduct(Product product) {
		productDAO.updateProduct(product);
	}

	@DELETE
	@Path("/{product_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteProduct(@PathParam("product_id") int product_id) {
		productDAO.deleteById(product_id);
	}
	
	@DELETE
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteProducts() {
		productDAO.deleteAllProducts();
	}
}