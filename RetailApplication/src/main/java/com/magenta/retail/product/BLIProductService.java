package com.magenta.retail.product;

import java.util.List;


public interface BLIProductService {
	
	public void addProduct(Product Product);
	
	public List<Product> getProducts();
	
	public Product getProductById(int Product_id);
	
	public void deleteProductById(int Product_id);
	
	public void updateProduct(Product Product);
	
	public void deleteAllProducts();
	
	public List<Product> getProductsByCategory(int categoryId);
}
