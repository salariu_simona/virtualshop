package com.magenta.retail.orderProduct;

import java.util.List;

public interface BLIOrderProductsService {

	public  List<OrderProducts> getOrders(int order_id);
	
	public void add(OrderProducts orderProduct);
	
	public void generatePDFWithOrder(int user_id, int orderId);
}
