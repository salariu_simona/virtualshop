package com.magenta.retail.runner;

import com.magenta.retail.logic.ApplicationLogic;
/**
 * The class where the application is launched
 *
 */
public class ApplicationRunner {
	public static void main(String[] args) {
		ApplicationLogic application = new ApplicationLogic();
		application.initializeShop();
		application.menu();
	}
	
}
