package com.magenta.retail.product;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Class for reading a list of product from a file
 *
 */
public class BLProductReader implements BLIProductReader {
	BufferedReader br;
	public List<Product> readFile(String fileName) {
		
		String[] fields;
		List<Product> listOfProducts = new ArrayList<Product>();
		try {
			br = new BufferedReader(new FileReader(new File(fileName)));
			String line;
			while ((line = br.readLine()) != null) {
				fields = line.split("/");
				listOfProducts.add(new Product.ProductBuilder(fields[0]).quantity(Integer.parseInt(fields[1]))
						.price(Double.parseDouble(fields[2])).build());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return listOfProducts;
	}
}
