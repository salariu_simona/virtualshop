package com.magenta.retail.mail;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.magenta.retail.customer.Customer;
import com.magenta.retail.customer.CustomerRepository;
import com.magenta.retail.orderProduct.BLOrderProductsService;
import com.magenta.retail.orderProduct.OrderProducts;
import com.magenta.retail.product.Product;

@Service
public class MailService {

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	MailContentBuilder mailContentBuilder;

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired	
	BLOrderProductsService productService;
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

	public void sendMailWithOrder(List<OrderProducts> orderProducts) {
		
		 
		Mail mail = new Mail();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage,true);
			String body = "";
			List<String> nameOfTheProducts = new ArrayList<>();
			List<Integer> quantityOfProducts = new ArrayList<>();
			int customerId = orderProducts.get(0).getOrder().getUser().getCustomerId();
			String customerName = orderProducts.get(0).getOrder().getUser().getFirstName();
			mail.setFrom("virtual.shop1@yahoo.com");
			Customer customer = customerRepository.findBycustomerId(customerId);
			mail.setTo(customer.getEmail());
			mail.setSubject(dateFormat.format(date).toString() + " About your order");
			messageHelper.setFrom(mail.getFrom());
			messageHelper.setTo(mail.getTo());
			messageHelper.setSubject(mail.getSubject());
			
			
			for(OrderProducts product : orderProducts) {
				nameOfTheProducts.add(product.getProduct().getName());
				quantityOfProducts.add(product.getQuantity());
			}
			for(int i = 0; i < nameOfTheProducts.size(); i++) {
				body += i+1 + ". " + "Name of the product: " + nameOfTheProducts.get(i) + ", Quantity:" + quantityOfProducts.get(i) + "\n";
			}
			  
			mail.setContent(body);		
			String content = mailContentBuilder.build(mail.getContent(), customerName);
			messageHelper.setText(content, true);
			FileSystemResource file = new FileSystemResource(new File("src/main/resources/Order.pdf"));
			TimeUnit.SECONDS.sleep(3);
			messageHelper.addAttachment("Order.pdf", file);
			logger.info("Sending email to " + mail.getTo());
		};
		try {
			javaMailSender.send(messagePreparator);
			logger.info("Done!");
		} catch (MailException e) {
			logger.error("Something went wrong....");
			logger.error(e.toString());
		}
	}

	public void sendMailWithSuggestion(int customerId, List<Product> listOfSuggestions) {
		Mail mail = new Mail();
		mail.setFrom("virtual.shop1@yahoo.com");
		Customer customer = customerRepository.findBycustomerId(customerId);
		mail.setTo(customer.getEmail());
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			String body = "";
			for(int i = 0 ; i < listOfSuggestions.size(); i++ ) {
				body += listOfSuggestions.get(i).getName() + "\n";
			}
			mail.setTo(customer.getEmail());
			mail.setSubject("[VirtualShop] Some suggestion about your last order");
			messageHelper.setFrom(mail.getFrom());
			messageHelper.setTo(mail.getTo());
			messageHelper.setSubject(mail.getSubject());
			mail.setContent(body);
			String content = mailContentBuilder.buildTemplateSuggestion(mail.getContent(), customer.getFirstName());
			messageHelper.setText(content, true);
			logger.info("Sending email with suggestions to -> " + mail.getTo());
		};
		try {
			javaMailSender.send(messagePreparator);
			logger.info("Done!");
		} catch (MailException e) {
			logger.error("Something went wrong sending email with suggestions....");
			logger.error(e.toString());
		}
	}
}
