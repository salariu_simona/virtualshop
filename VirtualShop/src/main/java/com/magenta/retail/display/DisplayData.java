package com.magenta.retail.display;

import java.util.List;
import java.util.Map;


import org.apache.log4j.Logger;
import com.magenta.retail.addressEntity.Address;
import com.magenta.retail.customerEntity.Customer;
import com.magenta.retail.orderEntity.Order;
import com.magenta.retail.product.Product;
/**
 * This class provides all the necessary methods to display messages on the console for helping customer to interract with the application
 *
 */
public class DisplayData {

	public static final Logger LOGGER=Logger.getLogger(DisplayData.class);
	
	public DisplayData() {
		
	}
	
	public void printMessage(String message) {
		LOGGER.info(message);
		
	}

	public void showAddress(String message,List<Address> list) {
		String details = message;
		for(Address a : list) {
			details +=  a.toString() + "\n\t\t\t\t\t\t\t  " ;
		}
		
		LOGGER.info(details);
		
	}
	
	public void showCustomers(String message,List<Customer> list) {
		String details = message;
		for(Customer a : list) {
			details +=  a.toString() + "\n\t\t\t\t\t\t\t\t";
		}
		
		LOGGER.info(details);	
	}
	public void showOrders(String message,List<Order> list) {
		String details = message;
		for(Order a : list) {
			details +=  a.toString() + "\n\t\t\t\t\t\t\t\t";
		}
		
		LOGGER.info(details);	
	}
	public void showProducts(String message,List<com.magenta.retail.productEntity.Product> listOfProducts) {
		String details = message;
		for(com.magenta.retail.productEntity.Product a : listOfProducts) {
			details +=  a.toString() + "\n\t\t\t\t\t\t\t\t";
		}
		
		LOGGER.info(details);	
	}
	// print list of products on console
	public void printListOfProducts( Map<Integer, Product> listOfProducts) {
		for(Map.Entry<Integer, Product> product:listOfProducts.entrySet()) {
			LOGGER.info(product.getKey() + "." + product.getValue().getName());
			LOGGER.info("Pret: " + product.getValue().getPrice());
		}
	}

	// print payment on console
	public void showPayment(double totalPrice, Map<Integer,Product> shoppingCart) {
		System.out.println("\t\t\t\t\tFACTURA DE PLATA\n");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------");
		System.out.printf("%10s %50s %23s", "DENUMIRE PRODUS", "CANTITATE", "PRET(buc)");
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------------------");
		for(Map.Entry<Integer, Product> product:shoppingCart.entrySet()) {
			System.out.format("%10s %8s %30s",product.getValue().getName(), product.getValue().getQuantity(),
					product.getValue().getPrice() + " lei");
			System.out.println();
		}
		System.out.println(
				"-------------------------------------------------------------------------------------------------------");
		System.out.printf("%100s", "PRET TOTAL: " + totalPrice + "lei\n");
	}
}
