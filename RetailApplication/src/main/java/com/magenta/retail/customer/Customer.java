package com.magenta.retail.customer;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magenta.retail.address.Address;
import com.magenta.retail.answer.Answer;
import com.magenta.retail.favourites.FavouriteProducts;
import com.magenta.retail.order.Order;
import com.magenta.retail.question.Question;
import com.magenta.retail.roles.Roles;
import com.magenta.retail.shoppingCart.ShoppingCart;


@Entity
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String password;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CUSTOMER_ADDRESS", 
				joinColumns = @JoinColumn(name = "customerId", referencedColumnName = "customerId"), 
				inverseJoinColumns = @JoinColumn(name = "addressId", referencedColumnName = "addressId"))
	private List<Address> addresses = new ArrayList<Address>();
	
	@OneToMany(cascade = CascadeType.PERSIST,mappedBy = "user")
	private List<Order> list_orders = new ArrayList<Order>();
	
	@OneToMany(cascade = CascadeType.PERSIST,mappedBy = "user")
	private List<Answer> list_answers = new ArrayList<Answer>();
	
	@OneToMany(cascade = CascadeType.PERSIST,mappedBy = "user")
	private List<ShoppingCart> shoppingCartList = new ArrayList<ShoppingCart>();
	
	@OneToMany(cascade = CascadeType.PERSIST,mappedBy = "user")
	private List<Question> questionList = new ArrayList<Question>();
	
	@OneToMany(cascade = CascadeType.PERSIST,mappedBy = "user")
	private List<FavouriteProducts> favouriteProducts = new ArrayList<>();
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "role_id")
	private Roles role; 

	
	public Customer() {};
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	@JsonIgnore
	public List<Order> getList_orders() {
		return list_orders;
	}

	public void setList_orders(List<Order> list_orders) {
		this.list_orders = list_orders;
	}

	@JsonIgnore
	public List<ShoppingCart> getShoppingCartList() {
		return shoppingCartList;
	}
	public void setShoppingCartList(List<ShoppingCart> shoppingCartList) {
		this.shoppingCartList = shoppingCartList;
	}
	public Roles getRole() {
		return role;
	}
	public void setRole(Roles role) {
		this.role = role;
	}
	@JsonIgnore
	public List<FavouriteProducts> getFavouriteProducts() {
		return favouriteProducts;
	}
	public void setFavouriteProducts(List<FavouriteProducts> favouriteProducts) {
		this.favouriteProducts = favouriteProducts;
	}
	public String toString() {
		String details = "{Customer id: " + this.customerId + ", First name: " + this.firstName + ", Last name: " + this.lastName + ",Email: " + this.email + ",Phone number: " + this.phoneNumber;
		return details;
	}
	@JsonIgnore
	public List<Question> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(List<Question> questionList) {
		this.questionList = questionList;
	}
	@JsonIgnore
	public List<Answer> getList_answers() {
		return list_answers;
	}
	public void setList_answers(List<Answer> list_answers) {
		this.list_answers = list_answers;
	}
	
	
	
	
}

	
		
