import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAnswerDialogComponent } from './admin-answer-dialog.component';

describe('AdminAnswerDialogComponent', () => {
  let component: AdminAnswerDialogComponent;
  let fixture: ComponentFixture<AdminAnswerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAnswerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAnswerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
