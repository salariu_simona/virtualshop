package com.magenta.retail.review;

import java.util.List;

public interface BLIReviewService {

	public void addReview(Review review);
	
	public List<Review> getReviewsForProduct(int productId);
	
}
