package com.magenta.retail.orderEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import com.magenta.retail.customerEntity.Customer;
import com.magenta.retail.productEntity.Product;


@Entity
@Table(name = "Orders")
@CascadeOnDelete
@XmlRootElement
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orderId", updatable = false, nullable = false) 
	private Integer orderId;
	private Integer quantity;
	private Double finalPrice;
	private String paymentMethod;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "customer", referencedColumnName = "customer_id")
	@CascadeOnDelete
	private Customer customer;
	
	 @ManyToMany(mappedBy = "orders")
	 @CascadeOnDelete
	  private List<Product> products = new ArrayList<Product>();
	
	// Constructors
	public Order() {
		super();
	}
	
	public String getPayment_method() {
		return paymentMethod;
	}



	public void setPayment_method(String payment_method) {
		this.paymentMethod = payment_method;
	}



	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public List<Product> getProducts() {
		return products;
	}


	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public void addProducts(Product product) {
		products.add(product);
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Order(Integer id, String firstName, String lastName, Integer quantity, Double finalPrice,
			Double shippingCost, String paymentMethod, String phoneNumber, String emailAdress, String zipCode) {
		super();
		this.orderId = id;
		this.quantity = quantity;
		this.finalPrice = finalPrice;
		this.paymentMethod = paymentMethod;
	}

	// Getters + setters
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	@Override
	public String toString() {
		String orderDetails = "{Order ID:" + orderId + ", order quantity=" + quantity + ", order final price=" + finalPrice + ", payment method="
				+ paymentMethod + ", customer: " + customer + "}";
		
		return orderDetails;
	}
}
