package com.magenta.retail.favourites;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magenta.retail.product.Product;

@Service
public class BlFavouriteProducts implements BliFavouriteProducts {
	
	@Autowired
	private FavouriteProductsRepository favouriteProductsRepository;

	@Override
	public void addFavouriteProduct(FavouriteProducts favouriteProducts) {
		FavouriteProducts existingFavouriteProduct = favouriteProductsRepository.findByuserCustomerIdAndProductProductId(favouriteProducts.getUser().getCustomerId(), favouriteProducts.getProduct().getProductId());
		if(existingFavouriteProduct == null) {
			favouriteProductsRepository.save(favouriteProducts);
		}
	}

	@Override
	public List<FavouriteProducts> getFavouriteProducts(int user_id) {
		return favouriteProductsRepository.findByuserCustomerId(user_id);
	}

	@Override
	public void deleteProductFromFavouriteProduct(int user_id, int product_id) {
		FavouriteProducts favouriteProducts = favouriteProductsRepository.findByuserCustomerIdAndProductProductId(user_id, product_id);
		favouriteProductsRepository.delete(favouriteProducts);
		
	}

	@Override
	public void deleteFavouriteProducts(int user_id) {
		List<FavouriteProducts> favouriteProducts = favouriteProductsRepository.findByuserCustomerId(user_id);
		for(int i = 0; i < favouriteProducts.size(); i++) {
			favouriteProductsRepository.delete(favouriteProducts.get(i));
		}
	}

	@Override
	public Product getProductByCustomerId(int user_id, int product_id) {
		return favouriteProductsRepository.findByuserCustomerIdAndProductProductId(user_id, product_id).getProduct();
	}
	
	

}
