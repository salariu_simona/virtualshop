import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router:Router) {
    this.ngOnInit();
   }

  ngOnInit() {
    this.logout();
  }

  logout(){
    this.router.navigate(['login']);
  }
}
