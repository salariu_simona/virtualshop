package com.magenta.retail.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.magenta.retail.utils.Utils;
/**
 * This class provides a list of products read from a file
 *
 */
public class ProductStock {
	private Map<Integer, Product> stockOfProducts;

	public ProductStock() {
		stockOfProducts = new HashMap<Integer, Product>();
	}

	public Map<Integer, Product> getStockOfProducts() {
		return stockOfProducts;
	}

	public void setStockOfProducts(Map<Integer, Product> stockOfProducts) {
		this.stockOfProducts = stockOfProducts;
	}

	// initialize stock of products
	public void initializeStockOfProducts() {
		BLIProductReader reader = new BLProductReader();
		List<Product> listOfProducts = new ArrayList<Product>();
		listOfProducts = reader.readFile(Utils.PRODUCTS_FILE_PATH);
		for (int i = 0; i < listOfProducts.size(); i++) {
			stockOfProducts.put(i + 1, listOfProducts.get(i));
		}
	}
	
	// remove product from stock when a clint buy it
	public void removeProductsFromStock(Map<Integer, Product> shoppingCart) {
		for (Map.Entry<Integer, Product> product : shoppingCart.entrySet()) {
			int key = product.getKey();
			int newQuantity = stockOfProducts.get(key).getQuantity() - product.getValue().getQuantity();
			Product newProduct = new Product.ProductBuilder(product.getValue().getName()).quantity(newQuantity)
					.price(product.getValue().getPrice()).build();
			stockOfProducts.put(key, newProduct);
		}
	}

	// update stock of products when a command is canceled
	public void updateStock(Map<Integer, Product> shoppingCart) {
		for (Map.Entry<Integer, Product> product : shoppingCart.entrySet()) {
			int key = product.getKey();
			int newQuantity = stockOfProducts.get(key).getQuantity() + product.getValue().getQuantity();
			Product newProduct = new Product.ProductBuilder(product.getValue().getName()).quantity(newQuantity)
					.price(product.getValue().getPrice()).build();
			stockOfProducts.put(key, newProduct);
		}

	}

	// method return quantity for a specific product
	public int getProductQuantity(int productIndex) {
		return stockOfProducts.get(productIndex).getQuantity();
	}

}
