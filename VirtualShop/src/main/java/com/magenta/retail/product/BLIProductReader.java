package com.magenta.retail.product;
import java.util.List;

public interface BLIProductReader {
	public List<Product> readFile(String fileName);
}
