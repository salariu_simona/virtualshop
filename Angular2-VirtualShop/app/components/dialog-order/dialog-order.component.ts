import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Order, OrderProduct } from '../../Order';
import { OrderService } from '../../services/order.service';
import { User } from '../../user';
import { Product } from '../../product';
import { ShoppingCart, ProductQuantityPair } from '../../shoppingcart';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { MailService } from '../../services/mail.service';
import { Category } from '../../category';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-dialog-order',
  templateUrl: './dialog-order.component.html',
  styleUrls: ['./dialog-order.component.css']
})
export class DialogOrderComponent implements OnInit {

  date: Date;
  private finalPrice: number;
  paymentMethod: string = "cash";
  order: Order;
  orderFound: Order;
  orderProduct: OrderProduct;
  orderProducts: OrderProduct[] = [];
  pair: ProductQuantityPair;
  products: ProductQuantityPair[] = new Array();
  errorMessage: string;
  categories:number[] = [];
  productsForSugestion: Product[] = [];
 
  constructor(private productService:ProductService,public dialogRef: MatDialogRef<DialogOrderComponent>, @Inject(MAT_DIALOG_DATA) public data: number, private orderService: OrderService, private shopService: ShoppingCartService, private mailService: MailService) {
    this.date = new Date();
    this.finalPrice = data;
  }

  ngOnInit() {
  }

  cancelButtonAction() {
    this.dialogRef.close();
  }
  onChange(deviceValue) {
    this.paymentMethod = deviceValue;
  }
  orderFinalize(deliveryAddress: string, finalPrice: number) {

    this.order = new Order();
    this.order.paymentMethod = this.paymentMethod;
    this.order.finalPrice = finalPrice;
    this.order.deliveryAddress = deliveryAddress;
    this.order.orderDate = this.date.toString();
    this.order.user = new User();
    this.order.user.customerId = parseInt(localStorage.getItem("userId"));
    
    if(this.order.deliveryAddress =='') {
      this.errorMessage = "Please Complete the field.";
    } else {
    this.orderService.insertOrder(this.order).subscribe((data: Order) => {
      this.orderFound = data;
      this.getProducts(this.orderFound.orderId);
    });
  }
}


  getProducts(orderId: number) {
    let user = parseInt(localStorage.getItem("userId"));
    this.shopService.getProductsFromShoppingCart(user).subscribe((shopingCart: ShoppingCart[]) => {
      shopingCart.forEach(element => {
        this.pair = new ProductQuantityPair();
        this.pair.product = element.product;
        this.pair.quantity = element.quantity;
        this.products.push(this.pair);
        this.orderProduct = new OrderProduct();
        this.orderProduct.order = new Order();
        this.orderProduct.order.orderId = orderId;
        this.orderProduct.order.user = new User();
        this.orderProduct.order.user.customerId = user;
        this.orderProduct.product = this.pair.product;
        this.orderProduct.quantity = this.pair.quantity;
        this.orderProducts.push(this.orderProduct);
        if(!this.categories.includes(this.pair.product.category.categoryId)) {
          this.categories.push(this.pair.product.category.categoryId);
        }
        this.orderService.insertOrderProduct(this.orderProduct).subscribe((res: Response) => { console.log(res); });
        this.cancelButtonAction();
        this.mailService.sendMailWithOrder(this.orderProducts).subscribe((res: Response) => { console.log(res); });
      }); 
      this.getProductsForSugestion();
    
    });
  }

  getProductsForSugestion(){
    this.categories.forEach(categoryId => {
      this.productService.getProductsByCategory(categoryId).subscribe((data:Product[]) => {
        data.forEach(product => {
          this.productsForSugestion.push(product);
        });   
         this.sendEmailWithSugestion(); 
      }) 
    });
  }

  sendEmailWithSugestion() {
    this.mailService.sendMailWithSugestion(this.productsForSugestion).subscribe((response: Response) => {
      console.log(response);
    });
    location.reload();
    location.reload();
    
  }
}