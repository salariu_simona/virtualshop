package com.magenta.retail.customerEntity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import com.magenta.retail.addressEntity.Address;
import com.magenta.retail.orderEntity.Order;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity
@Table
@CascadeOnDelete
@XmlRootElement
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customer_id;
	private String first_name;
	private String last_name;
	private String phone_number;
	private String email;
	private String password;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CUSTOMER_ADDRESS", 
				joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "customer_id"), 
				inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "address_id"))
	@CascadeOnDelete
	private List<Address> addresses = new ArrayList<Address>();

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "customer")
	@CascadeOnDelete
	private List<Order> list_orders = new ArrayList<Order>();

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public void addOrder(Order order) {
		list_orders.add(order);
	}
	
	public List<Order> getList_orders() {
		return list_orders;
	}

	public void setList_orders(List<Order> list_orders) {
		this.list_orders = list_orders;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public void addCostumerAddress(Address costumerAddress) {
		addresses.add(costumerAddress);
	}
	
	public String toString() {
		String details = "{Customer id: " + this.customer_id + ", First name: " + this.first_name + ", Last name: " + this.last_name + ",Email: " + this.email + ",Phone number: " + this.phone_number;
		if(addresses.size() != 0) {
			details +=  ", Address List = " + addresses + "}";
		} else {
			details += "}";
		}
		return details;
	}
}
