package com.magenta.retail.address;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magenta.retail.customer.Customer;

@Entity
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int addressId;
	private String street_name;
	private String number;
	private String city;
	private String country;
	@ManyToMany(mappedBy = "addresses")
	private List<Customer> customers = new ArrayList<Customer>();
	public int getAddressId() {
		return addressId;
	}
	public Address() {};
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public String getStreet_name() {
		return street_name;
	}

	public void setStreet_name(String street_name) {
		this.street_name = street_name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@JsonIgnore
	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "{Address ID:" + this.addressId + ", Street Name: " + this.street_name + ", Number: " + this.number + ", City: " + this.city + ", Country: " + this.country + "}";
	}

}