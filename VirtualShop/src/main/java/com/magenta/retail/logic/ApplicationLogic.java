package com.magenta.retail.logic;

import com.magenta.retail.display.*;
import com.magenta.retail.product.*;
import com.magenta.retail.utils.Utils;
/**
 * This class provides all the logical part of this application.
 * This contains a list of products, a list of money from the store's budge and all the necessary classes for successful purchase of products
 *
 */

public class ApplicationLogic {

	private ProductStock productStock;
	private ShoppingCart clientShoppingCart;
	private DisplayData displayData;
	private PaymentProcess paymentProcess;
	private ReaderClass input;

	// method for initialize the shop, the stock of products, the stock of money
	public void initializeShop() {
		input = new ReaderClass();
		paymentProcess = new PaymentProcess();
		productStock = new ProductStock();
		clientShoppingCart = new ShoppingCart();
		displayData = new DisplayData();
		productStock.initializeStockOfProducts();
	}

	// menu method for interaction with client
	public void menu() {
		shopInformations();
		displayData.printMessage(Utils.SHOPPING_PRODUCTS_MESSAGE);
		int clientChoice = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, 2);
		if (clientChoice == 2) {
			leaveApplication();
		}
		while (clientChoice == 1) {
			choiceProductAndAddToCart();
			displayData.printMessage(Utils.SHOPPING_ANOTHER_PRODUCTS_MESSAGE);
			clientChoice = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, 2);
		}
		if (paymentConfirmation() == true) {
			paymentMethod(paymentProcess.formatNumber(clientShoppingCart.getTotalPrice()));
		} else {
			productStock.updateStock(clientShoppingCart.getShoppingCart());
			leaveApplication();
		}
	}

	// display informations about shop, list of available products
	public void shopInformations() {
		displayData.printMessage(Utils.WELCOME_MESSAGE);
		displayData.printMessage(Utils.LIST_OF_AVAILABLE_PRODUCTS);
		displayData.printListOfProducts(productStock.getStockOfProducts());
	}

	// method for helping the client to select index and quantity for the desired product and then adding it to the shopping cart
	public void choiceProductAndAddToCart() {
		displayData.printMessage(Utils.CHOICE_PRODUCTS_INDEX_MESSAGE);
		int productIndex = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, productStock.getStockOfProducts().size());
		if (productStock.getProductQuantity(productIndex) == 0) {
			choiceAnotherProduct();
		} else {
			displayData.printMessage(Utils.CHOICE_PRODUCTS_QUANTITY_MESSAGE);
			int numberOfProducts = input.readIntValue(Utils.INVALID_QUANTITY_MESSAGE, 1,
					productStock.getProductQuantity(productIndex));
			clientShoppingCart.addToShoppingCart(productStock, productIndex, numberOfProducts);
			productStock.removeProductsFromStock(clientShoppingCart.getShoppingCart());
		}
	}

	// if the stock don't has the desired product, he is notified and asked about another product
	public void choiceAnotherProduct() {
		displayData.printMessage(Utils.STOCK_NOT_AVAILABLE_MESSAGE);
		int choice = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, 2);
		if (choice == 1) {
			choiceProductAndAddToCart();
		} else {
			leaveApplication();
		}
	}

	// return true if the client choose to finalize the command
	public boolean paymentConfirmation() {
		displayData.showPayment(paymentProcess.formatNumber(clientShoppingCart.totalPriceCalculation().getTotalPrice()
				),
				clientShoppingCart.getShoppingCart());
		displayData.printMessage(Utils.PAYMENT_CONFIRMATION_MESSAGE);
		int clientChoice = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, 2);
		if (clientChoice == 1) {
			return true;
		} else {
			return false;
		}
	}

	// method for adding the posibility to pay in RON, EURO or DOLLAR
	public void paymentMethod(double totalPrice) {

		displayData.printMessage(Utils.CURRENCY_OPTION_MESSAGE);
		int choice = input.readIntValue(Utils.INVALID_OPTION_MESSAGE, 1, 3);
		if (choice == 1) {
			paymentProcess.paymentProcessWithRon(totalPrice);
		} else if (choice == 2) { 
			paymentProcess.paymentProcess("Euro", totalPrice);
		} else if (choice == 3) {
			paymentProcess.paymentProcess("Dollar", totalPrice);
		}
		leaveApplication();
	}

	// the method is called when the client leave the application
	public void leaveApplication() {
		displayData.printMessage("Va multumim!");
		clientShoppingCart.deleteProductsFromCart();
		clientShoppingCart.setTotalPrice(0);
		menu();
	}

}
