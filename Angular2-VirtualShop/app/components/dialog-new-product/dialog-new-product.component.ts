import { Component, OnInit } from '@angular/core';
import {Product} from "../../product";
import {DialogComponent} from "../dialog/dialog.component";
import {MatDialogRef} from "@angular/material";
import {ProductService} from "../../services/product.service";
import { CategoryService } from '../../services/category.service';
import { Category } from '../../category';

@Component({
  selector: 'app-dialog-new-product',
  templateUrl: './dialog-new-product.component.html',
  styleUrls: ['./dialog-new-product.component.css']
})
export class DialogNewProductComponent implements OnInit {
  private product: Product;
  errorMessage: string;
  categorySelected: string;
  categoryId: number;
  categories: Category[];

  constructor(private categoryService: CategoryService,private productService: ProductService,public dialogRef : MatDialogRef<DialogComponent>) { }

  ngOnInit() {
    this.getCategories();
  }

  cancelButtonAction() {
    this.dialogRef.close();
  }

  insertButtonAction(name: string,price: string, quantity: string,photo: string) {
    this.product = new Product();
    if(parseFloat(price) <= 0) {
      this.errorMessage = 'Price can not be zero or a negative number.'
    } else if(parseInt(quantity) < 0 ) {
      this.errorMessage = 'Quantity can not be a negative number.';
    } else if(name == '' || price == '' || quantity == '') {
      this.errorMessage = "Please complete all fields that are required."
    } else {
    this.product.name = name;
    this.product.price = parseFloat(price);
    this.product.quantity = parseInt(quantity);
    if(photo != '') {
      this.product.photo = photo;
    } 
    else {
      this.product.photo = "not-available.png";
    }
     
    this.product.category = new Category();
    this.product.category.categoryId = this.categoryId;
    this.product.category.categoryName = this.categorySelected;
    this.productService.insertProduct(this.product).subscribe((res:Response)=> {
    window.location.reload();
    console.log(res); });
    this.cancelButtonAction();
    }
  }

  getCategories() {
    this.categoryService.findAllCategories().subscribe((data: Category[]) => { this.categories = data;
      console.log(this.categories); 
      this.categorySelected = this.categories[0].categoryName;
      this.categoryId = this.categories[0].categoryId;
      
    });
  }

  onChange(deviceValue) {
    this.categorySelected = deviceValue;
    this.categories.forEach(category => {
      if(category.categoryName == deviceValue) {
         this.categoryId = category.categoryId;
         this.categorySelected = category.categoryName;
      }  
    });
  }
  

}
