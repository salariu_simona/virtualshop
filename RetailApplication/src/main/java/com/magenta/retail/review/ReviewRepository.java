package com.magenta.retail.review;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ReviewRepository extends CrudRepository<Review,Integer>{

	public List<Review> findByproductProductId(int productId);
}
