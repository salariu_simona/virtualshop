package com.magenta.retail.answer;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface AnswerRepository extends CrudRepository<Answer,Integer>{
	public List<Answer> findByquestionQuestionId(int questionId);

}
