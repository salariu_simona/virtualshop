package com.magenta.retail.shoppingCart;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ShoppingCartRepository extends CrudRepository<ShoppingCart,Integer> {

	public List<ShoppingCart> findByuserCustomerId(int user_id);
	public ShoppingCart findByuserCustomerIdAndProductProductId(int user_id,int product_id);
}
