import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { Product } from '../product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string = 'http://localhost:9000/product/';

  constructor(private http: Http) {
      console.log("Product service is initialized..");
   }

  public findAllProducts() {
     return this.http.get(this.url + 'all').pipe(map((response:Response)=>response.json()));
   }

  public deleteProducts(id: number){
    console.log("delete id:" + id);
    return this.http.delete(this.url + 'delete/' + id).pipe(map((response:Response)=>response.status));
   }

  public updateProduct(product: Product) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.url + 'update', product).pipe(map((response:Response)=>
    {(<any>response)._body =='' ? null : response.json()}));
   }

   public insertProduct(product: Product) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.put(this.url + 'add',product).pipe(map((response: Response)=>response.status));
   }

   public getProductById(id: number) {
     return this.http.get(this.url + id).pipe(map((response:Response)=>response.json()));
   }

   public getProductsByCategory(categoryId: number) {
    return this.http.get(this.url + 'category/' + categoryId).pipe(map((response:Response)=>response.json()));
   }
}
