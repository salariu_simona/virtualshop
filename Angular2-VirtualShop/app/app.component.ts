import { Component,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog} from "@angular/material";
import { DialogNewProductComponent } from "./components/dialog-new-product/dialog-new-product.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  
  constructor(public router: Router,public dialog: MatDialog){}

  openDialog() {
    const dialogRef = this.dialog.open(DialogNewProductComponent, {
      width: '500px',
      height: '550px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getTypeOfAccess(){
    return localStorage.getItem("typeOfAccess");
  }
}
