package com.magenta.retail.orderProduct;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface OrderProductRepository extends CrudRepository<OrderProducts,Integer>{

	public List<OrderProducts> findByOrderOrderId(int order_id);
}
