import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { User } from '../user';
import { Category } from '../category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  url: string = 'http://localhost:9000/category/';

  constructor(private http: Http) { }

  public findAllCategories() {
    return this.http.get(this.url + 'all').pipe(map((response:Response)=>response.json()));
  }

  public findByCategoryId(id: number) {
    return this.http.get(this.url + id)
              .pipe(map((response : Response) => response.json()));
  }


}
