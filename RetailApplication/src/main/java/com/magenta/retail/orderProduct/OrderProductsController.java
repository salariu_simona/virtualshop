package com.magenta.retail.orderProduct;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orderProduct")
public class OrderProductsController {
	
	@Autowired
	BLOrderProductsService orderProductService;
	
	@RequestMapping(value = "/{order_id}",method = RequestMethod.GET)
	public  List<OrderProducts> getOrderProducts(@PathVariable("order_id") int order_id) {
		return orderProductService.getOrders(order_id);
	}
	
	@RequestMapping(value = "/add",method = RequestMethod.PUT)
	public void insert(@RequestBody OrderProducts orderProduct) {
		orderProductService.add(orderProduct);
	}
}
