package com.magenta.retail.answer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BLAnswerService implements BLIAnswerService{

	@Autowired
	AnswerRepository answerRepository;
	
	public void addAnswer(Answer answer) {
		answerRepository.save(answer);
		
	}

	@Override
	public List<Answer> getAnswersForQuestion(int questionId) {
		return answerRepository.findByquestionQuestionId(questionId);
	}

}
