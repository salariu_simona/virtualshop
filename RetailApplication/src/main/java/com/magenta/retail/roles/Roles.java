package com.magenta.retail.roles;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magenta.retail.customer.Customer;

@Entity(name = "Role")
public class Roles {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int roleId;
	private String typeOfAccess;

	@OneToMany(mappedBy = "role",cascade = CascadeType.MERGE)
	private List<Customer> users = new ArrayList<Customer>();

	@PrePersist
	public void prePersist() {
		if(typeOfAccess == null)
			typeOfAccess = "user";
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getTypeOfAccess() {
		return typeOfAccess;
	}

	public void setTypeOfAccess(String typeOfAccess) {
		this.typeOfAccess = typeOfAccess;
	}

	@JsonIgnore
	public List<Customer> getUsers() {
		return users;
	}

	public void setUsers(List<Customer> users) {
		this.users = users;
	}
	

	

}
