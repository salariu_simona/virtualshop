import { Component, OnInit } from '@angular/core';
import { Product } from '../../product';
import { ProductService } from '../../services/product.service';
import { Router } from '@angular/router';

import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { Category } from '../../category';
import { CategoryService } from '../../services/category.service';
import { LoginComponent } from '../login/login.component';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  private categories: Category[];
  private products: Product[];
  private flag: boolean;

  ngOnInit() {
    this.products = [];
    this.allProducts();
    this.getCategories();
  }
  constructor(private categoryService:CategoryService,private productService: ProductService,private router: Router,public dialog: MatDialog) { 
  
  }

  public allProducts() {
    this.productService.findAllProducts().subscribe((data: Product[]) => { this.products = data;
      console.log(this.products); });
  }
  public deleteProduct(id: number) {
    this.productService.deleteProducts(id).subscribe((res: Response) => {
      this.allProducts();
    });
  }

  setProductId(id: number) {
    localStorage.setItem("id",id.toString());
    this.router.navigate(["productDetails"]);
  }

  openDialog(product: Product) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '500px', 
      height: '530px',
      data : { prod : product}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getTypeOfAccess(){
    return localStorage.getItem("typeOfAccess");
  }

  getCategories() {
    this.categoryService.findAllCategories().subscribe((data: Category[]) => { this.categories = data;
       
    });
  }

  getProductsByCategory(categoryId: number) {
    this.productService.getProductsByCategory(categoryId).subscribe((data: Product[]) => {
      this.products = data
      console.log(data);
    });
   
  }


}
