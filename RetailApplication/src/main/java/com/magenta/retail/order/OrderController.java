package com.magenta.retail.order;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;


@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	BliOrderService orderService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public Order addOrder(@RequestBody Order order) {
		return orderService.insertOrder(order);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Order getById(@PathVariable("id") @NotNull int id) {
		return orderService.findOrderById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@JsonIgnore
	public List<Order> getOrderes() {
		return orderService.findAllOrders();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Order updateOrder(@RequestBody Order Order) {
		return orderService.updateOrder(Order);
	}
	
	@RequestMapping(value = "/delete/all", method = RequestMethod.DELETE)
	public void deleteAllOrderes() {
		orderService.deleteAllOrders();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteOrderById(@PathVariable @NotNull int id) {
		orderService.deleteOrderById(id);
	}

}
