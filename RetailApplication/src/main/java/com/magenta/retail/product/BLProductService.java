package com.magenta.retail.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BLProductService implements BLIProductService{

	@Autowired
	private ProductRepository product_repository;
	
	public void addProduct(Product Product) {
		product_repository.save(Product);
	}
	public List<Product> getProducts() {
		return (List<Product>) product_repository.findAll();
	}
	
	public void deleteProductById(int Product_id) {
		product_repository.deleteById(Product_id);
	}
	
	public Product getProductById(int Product_id) {
		return product_repository.getByProductId(Product_id);	
	}
	
	public void updateProduct(Product Product) {
		if(product_repository.findById(Product.getProductId()) != null)
			product_repository.save(Product);
	}
	
	public void deleteAllProducts() {
		product_repository.deleteAll();
		
	}
	public List<Product> getProductsByCategory(int categoryId){
		return product_repository.getBycategoryCategoryId(categoryId);
	}
}
