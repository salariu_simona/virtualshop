package com.magenta.retail.roles;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/role")
public class RolesController {
	
	@Autowired
	private BliRolesService roleService;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public void addRole(@RequestBody Roles role) {
		roleService.addRole(role);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Roles> getAllRoles() {
		return roleService.getRoles();
	}
	
	@RequestMapping(value = "/{roleId}", method = RequestMethod.GET)
	public Roles getRole(@PathVariable("roleId") int roleId) {
		return roleService.getRoleById(roleId);
	}
	
	@RequestMapping(value = "/delete/{roleId}", method = RequestMethod.DELETE)
	public void deleteRole(@PathVariable("roleId") int roleId) {
		roleService.deleteRoleById(roleId);
	}
	
	@RequestMapping(value = "/delete/all", method = RequestMethod.DELETE)
	public void deleteRoles() {
		roleService.deleteAllRoles();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void updateRole(@RequestBody Roles role) {
		roleService.updateRole(role);
	}
	
}
