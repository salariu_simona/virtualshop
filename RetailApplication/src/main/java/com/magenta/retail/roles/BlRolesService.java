package com.magenta.retail.roles;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlRolesService implements BliRolesService{

	@Autowired
	private RolesRepository roleRepository; 

	@Override
	public void addRole(Roles role) {
		roleRepository.save(role);
	}

	@Override
	public List<Roles> getRoles() {
		return (List<Roles>) roleRepository.findAll();
	}

	@Override
	public Roles getRoleById(int roleId) {
		return roleRepository.getByroleId(roleId);
	}

	@Override
	public void deleteRoleById(int roleId) {
		roleRepository.deleteById(roleId);
	}

	@Override
	public void updateRole(Roles role) {
		if(roleRepository.findById(role.getRoleId()) != null)
			roleRepository.save(role);
	}

	@Override
	public void deleteAllRoles() {
		roleRepository.deleteAll();
	}

}
