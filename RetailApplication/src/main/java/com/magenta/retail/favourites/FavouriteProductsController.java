package com.magenta.retail.favourites;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/favouriteProducts")
public class FavouriteProductsController {
	
	@Autowired
	private BlFavouriteProducts blFavouriteProducts;
	
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public void addFavouriteProduct(@RequestBody FavouriteProducts favouriteProduct) {
		blFavouriteProducts.addFavouriteProduct(favouriteProduct);
	}
	
	@RequestMapping(value = "/all/{user_id}", method = RequestMethod.GET)
	public List<FavouriteProducts> getFavouriteProductById(@PathVariable("user_id") int user_id) {
		return blFavouriteProducts.getFavouriteProducts(user_id);
	}
	
	@RequestMapping(value = "/{user_id}/{product_id}", method = RequestMethod.GET)
	public boolean getProductByCustomerId(@PathVariable("user_id") int user_id, @PathVariable("product_id") int product_id) {
		try {
			blFavouriteProducts.getProductByCustomerId(user_id, product_id);
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}
	
	@RequestMapping(value = "/delete/{user_id}", method = RequestMethod.DELETE)
	private void deleteFavouriteProducts(@PathVariable int user_id) {
		blFavouriteProducts.deleteFavouriteProducts(user_id);
	}
	
	@RequestMapping(value = "/delete/{user_id}/{product_id}", method = RequestMethod.DELETE)
	private void deleteProductFromFavouriteProducts(@PathVariable("user_id") int user_id, @PathVariable("product_id") int product_id) {
		blFavouriteProducts.deleteProductFromFavouriteProduct(user_id, product_id);
	}
	
}
