package com.magenta.retail.favourites;

import java.util.List;

import com.magenta.retail.product.Product;

public interface BliFavouriteProducts {
	
	public void addFavouriteProduct(FavouriteProducts favouriteProducts);
	public List<FavouriteProducts> getFavouriteProducts(int user_id);
	public void deleteProductFromFavouriteProduct(int user_id,int product_id);
	public void deleteFavouriteProducts(int user_id);
	public Product getProductByCustomerId(int user_id, int product_id);
}