import { Component, OnInit } from '@angular/core';
import { User, Role } from '../../user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users : User[];
  role: Role;
  new_user: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.findAllUsers();
  }

  findAllUsers(){
    this.userService.findAllUsers().subscribe((data: User[]) => {this.users= data;

    });
  }

  changeType(email:string)
  {
     this.new_user = new User();
     this.new_user.email = email;
     this.userService.getUser(this.new_user).subscribe((user: User) => {
      console.log(user);
      this.role = new Role();
      if(user.role.typeOfAccess == "user") {
        this.role.roleId = 2;
        this.role.typeOfAccess = "admin";
      } else {
        this.role.roleId = 1;
        this.role.typeOfAccess = "user";
      }
      user.role = this.role;
      console.log(user);
      this.updateUser(user);
      location.reload();
    });
  }

  updateUser(user: User) {
    this.userService.updateUser(user).subscribe((res: Response)  => {
      console.log(res);
    });
  }
}


